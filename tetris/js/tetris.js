// Create the canvas
var canvas = document.createElement("canvas");
var ctx = canvas.getContext("2d");
canvas.width = 854;
canvas.height = 480;
canvas.id = "theCanvas";
document.body.appendChild(canvas);

//Global variables
var startGameSpeed = 500; //CHOOSE THE STARTING GAME SPEED: MILLISECONDS BETWEEN MOVES
var gameSpeed = startGameSpeed;
var speedMaxIncrease = startGameSpeed*2/3;
var level = 1;
var maxLevel = 10;
var gameScore = 0;
var gamePaused = false;
var gameFont = "BadRobot";
var gameOver = false; //for debugging purpose only

//game record globals
var gameLocalRecord = localStorage.tetrisRecord;
var newRecord = false;

//blink effect globals
var redLines = false; //for the lines to be red only in certain moments
var blinkHowLong = 150;
var piecesMovableLeftWhileBlink = true; 
var piecesMovableRightWhileBlink = true;
var piecesCanRotateWhileBlink = true;

//and here Global Variables for rendering purpose
var pixDim = canvas.height/24;
var potX = canvas.width/2-pixDim*6; //from the left
var potY = canvas.height/16;//from the top
var potWidth = 10*pixDim;
var potHeight = 20*pixDim;

//where pieces start falling
var startPositionX = 4;
var startPositionY = 18;

//generates a random number in between limits
function random(min, max) {
	return (max-min)*Math.random()+min;
}

//Event listener: record if keys are down
var keysDown = {};

addEventListener("keydown", function (e) {keysDown[e.keyCode] = true;}, false);
addEventListener("keyup", function (e) {delete keysDown[e.keyCode];}, false);

//here the game
var newGame = function () {
	//reset level and gamespeed if the game restarts
	level = 1;
	gamespeed = startGameSpeed;
	
	//update game score and game record
	gameScore = 0;
	gameLocalRecord = localStorage.tetrisRecord;
	newRecord = false;	
	
	//check if it's the first time playing and set the game record
	if (gameLocalRecord == undefined) {
		gameLocalRecord = 0;
	} 
	
	//here globals for the game statistics
	var manyI = 0;
	var manyJ = 0;
	var manyL = 0;
	var manyO = 0;
	var manyS = 0;
	var manyT = 0;
	var manyZ = 0;
	
	var manyTot = 0; 
	var manyIper = 0;
	var manyJper = 0;
	var manyLper = 0;
	var manyOper = 0;
	var manySper = 0;
	var manyTper = 0;
	var manyZper = 0;
	
	function updateStats() {
		manyTot = manyI+manyJ+manyL+manyO+manyS+manyT+manyZ;
		manyIper = manyI/manyTot*100;
		manyJper = manyJ/manyTot*100;
		manyLper = manyL/manyTot*100;
		manyOper = manyO/manyTot*100;
		manySper = manyS/manyTot*100;
		manyTper = manyT/manyTot*100;
		manyZper = manyZ/manyTot*100;	
	}
	

	//it's a random piece generator. Returns a letter I J L O S T Z out of a number between 1..7
	function generateRandomPiece() {
		var pieceNum = Math.floor(Math.random()*7+1);
		var pieceLetter = 0;
		switch (pieceNum) {
			case 1:
				pieceLetter = "I"; //I piece
				manyI++;
				break;
			case 2:
				pieceLetter = "J"; //JavaArray piece
				manyJ++;
				break;
			case 3:
				pieceLetter = "L"; //L piece
				manyL++;
				break;
			case 4:
				pieceLetter = "O"; //O piece
				manyO++;
				break;
			case 5:
				pieceLetter = "S"; //S piece
				manyS++;
				break;
			case 6:
				pieceLetter = "T"; //T piece
				manyT++;
				break;
			case 7:
				pieceLetter = "Z"; //Z piece
				manyZ++;
				break;
			default:
				console.log("Careful, generateRandomPiece() couldn't return an appropiate letter");
		}
		return pieceLetter;
	}
	
	//Create objects and specify their properties
		
	// ***POT***
	//here the pot, the game enviroment, an array that takes note of where the pieces are
	var pot = new Array();
	for (i=0; i<10; i++) { //10 columns, here it works because the loop goes from 0...9
		pot[i] = new Array(19); //and here 20 lines
	}
	//make each box postion in the pot void, equal to 0
	for (i=0; i<10; i++) {
		for (j=0; j<20; j++) {
			pot[i][j] = 0;
		}
	}
		
	// *** Next Piece ***
	var nextPiece = new Object();
	nextPiece.letter = generateRandomPiece();
	
	//here the piece object
	var piece = new Object();
	piece.letter = generateRandomPiece();
	piece.boxes = new Array(3);
	piece.positionX = startPositionX;
	piece.positionY = startPositionY;
	for (i=0; i<4; i++) {
		piece.boxes[i] = new Array(2); 
	}
	
	function updatePieceBoxesValues(letter) {
		switch (letter) {
			// colors are picked respecting the standard provided by The Tetris Company founded by Pajitnov
			case "I":
				piece.boxes[0][0]= -1;
				piece.boxes[0][1]= 0;
				piece.boxes[1][0]= 0;
				piece.boxes[1][1]= 0;
				piece.boxes[2][0]= 1;
				piece.boxes[2][1]= 0;
				piece.boxes[3][0]= 2;
				piece.boxes[3][1]= 0;
				break;
			case "J":
				piece.boxes[0][0]= -1;
				piece.boxes[0][1]= 0;
				piece.boxes[1][0]= 0;
				piece.boxes[1][1]= 0;
				piece.boxes[2][0]= 1;
				piece.boxes[2][1]= 0;
				piece.boxes[3][0]= 1;
				piece.boxes[3][1]= -1;
				break;
			case "L":
				piece.boxes[0][0]= -1;
				piece.boxes[0][1]= -1;
				piece.boxes[1][0]= -1;
				piece.boxes[1][1]= 0;
				piece.boxes[2][0]= 0;
				piece.boxes[2][1]= 0;
				piece.boxes[3][0]= 1;
				piece.boxes[3][1]= 0;
				break;
			case "O":
				piece.boxes[0][0]= 0;
				piece.boxes[0][1]= 0;
				piece.boxes[1][0]= 0;
				piece.boxes[1][1]= -1;
				piece.boxes[2][0]= 1;
				piece.boxes[2][1]= 0;
				piece.boxes[3][0]= 1;
				piece.boxes[3][1]= -1;
				
				break;
			case "S":
				piece.boxes[0][0]= -1;
				piece.boxes[0][1]= -1;
				piece.boxes[1][0]= 0;
				piece.boxes[1][1]= -1;
				piece.boxes[2][0]= 0;
				piece.boxes[2][1]= 0;
				piece.boxes[3][0]= 1;
				piece.boxes[3][1]= 0;
				break;
			case "T":
				piece.boxes[0][0]= -1;
				piece.boxes[0][1]= 0;
				piece.boxes[1][0]= 0;
				piece.boxes[1][1]= 0;
				piece.boxes[2][0]= 1;
				piece.boxes[2][1]= 0;
				piece.boxes[3][0]= 0;
				piece.boxes[3][1]= -1;
				
				break;
			case "Z":
				piece.boxes[0][0]= -1;
				piece.boxes[0][1]= 0;
				piece.boxes[1][0]= 0;
				piece.boxes[1][1]= 0;
				piece.boxes[2][0]= 0;
				piece.boxes[2][1]= -1;
				piece.boxes[3][0]= 1;
				piece.boxes[3][1]= -1;
				
				break;
			default:
				ctx.fillStyle = "rgba(0, 0, 0, 0)";
				console.log("Careful, while asigning piece.boxes default values the letter didn't match");
				
		}
	
	}
	
	updatePieceBoxesValues(piece.letter);
		
	function updatePiecePosition(positionX, positionY) {
		for (i=0; i<4; i++) {
			piece.boxes[i][0] = piece.boxes[i][0]+positionX;
		}
		for (i=0; i<4; i++) {
			piece.boxes[i][1] = piece.boxes[i][1]+positionY;
		}
	}
	
	function updateBackPiecePosition(positionX, positionY) {
		for (i=0; i<4; i++) {
			piece.boxes[i][0] = piece.boxes[i][0]-positionX;
		}
		for (i=0; i<4; i++) {
			piece.boxes[i][1] = piece.boxes[i][1]-positionY;
		}
	}
		
	updatePiecePosition(piece.positionX, piece.positionY);

	function moveDownThePiece() {		
		var movable = true;
		updateBackPiecePosition(piece.positionX, piece.positionY);
		piece.positionY = piece.positionY-1;
		updatePiecePosition(piece.positionX, piece.positionY);
		//check if the piece is touching one other piece
		for (i=0; i<4; i++) {
			if (
				(pot[piece.boxes[i][0]][piece.boxes[i][1]] == 0) || 
				(pot[piece.boxes[i][0]][piece.boxes[i][1]] == null)
			) {
				movable = true;
			} else {
				movable = false;
				break;
			}
		}
		//check if the piece reached the bottom and make it not movable
		if (movable) { //but only if it's still movable
			for (i=0; i<4; i++) {
				if (piece.boxes[i][1] >= 0) {
					movable = true;
				} else {
					movable = false;
					break;
				}
			}
		}
		if (!movable) { //if it was not possible to move it...
			updateBackPiecePosition(piece.positionX, piece.positionY);
			piece.positionY = piece.positionY+1; //...get it back to the previous position and...
			updatePiecePosition(piece.positionX, piece.positionY);
			//...draw the piece as permanent in the pot...
			drawPieceInPot();
			//...and grab next piece 
			grabNextPiece();
			//and set a block so that the next piece doesn't move down fast if [down key 40] is still pressed
			piecesMovableDownFast = false;
		}
	}
	
	function movePieceLeft() {
		var movableLeft = true;
		//move left
		//get every box back to default
		for (i=0; i<4; i++) {
			piece.boxes[i][0] = piece.boxes[i][0]-piece.positionX;
		}
		//move position left
		piece.positionX--;
		piecesMovableLeft = false;
		//update everybox position back to new position
		for (i=0; i<4; i++) {
			piece.boxes[i][0] = piece.boxes[i][0] + piece.positionX;
		}
		//check if it's inside the pot	
		for (i=0; i<4; i++) {
			if (piece.boxes[i][0]>=0) {
				movableLeft = true;
			} else {
				movableLeft = false;
				break;
			}
		}
		if (movableLeft) {
			for (i=0; i<4; i++) {
				if (
					(pot[piece.boxes[i][0]][piece.boxes[i][1]] == 0) || 
					(pot[piece.boxes[i][0]][piece.boxes[i][1]] == null)
				) {
					movableLeft = true;
				} else {
					movableLeft = false;
					break;
				}
			}
		}
		//if not get it back
		if (!movableLeft) {
			//get every box back to default
			for (i=0; i<4; i++) {
				piece.boxes[i][0] = piece.boxes[i][0]-piece.positionX;
			}
			//move position back
			piece.positionX++;
			piecesMovableLeft = true;
			//update everybox position back to new position
			for (i=0; i<4; i++) {
				piece.boxes[i][0] = piece.boxes[i][0] + piece.positionX;
			}
		}
	}
	
	function movePieceRight() {
		var movableRight = true;
		//move left
		//get every box back to default
		for (i=0; i<4; i++) {
			piece.boxes[i][0] = piece.boxes[i][0]-piece.positionX;
		}
		//move position right
		piece.positionX++;
		piecesMovableRight = false;
		//update everybox position back to new position
		for (i=0; i<4; i++) {
			piece.boxes[i][0] = piece.boxes[i][0] + piece.positionX;
		}
		//check if it's inside the pot	
		for (i=0; i<4; i++) {
			if (piece.boxes[i][0]<10) {
				movableRight = true;
			} else {
				movableRight = false;
				break;
			}
		}
		if (movableRight) {
			for (i=0; i<4; i++) {
				if (
					(pot[piece.boxes[i][0]][piece.boxes[i][1]] == 0) || 
					(pot[piece.boxes[i][0]][piece.boxes[i][1]] == null)
				) {
					movableRight = true;
				} else {
					movableRight = false;
					break;
				}
			}
		}
		//if not get it back
		if (!movableRight) {
			for (i=0; i<4; i++) {
				piece.boxes[i][0] = piece.boxes[i][0]-piece.positionX;
			}
			//move position right
			piece.positionX--;
			piecesMovableRight = true;
			//update everybox position back to new position
			for (i=0; i<4; i++) {
				piece.boxes[i][0] = piece.boxes[i][0] + piece.positionX;
			}
		}
	}
	
	function rotatePiece() {
		if (piece.letter != "O") {
			var rotatable = true; // :), yes rotatable
			//rotate
			for (i=0; i<4; i++) {
				var x = piece.boxes[i][0];
				var y = piece.boxes[i][1];

				x = x-piece.positionX;
				y = y-piece.positionY;

				//rotate
				var newX = -y;
				var newY = x;
				piecesCanRotate = false;

				//put new coors back in position
				var newX = newX+piece.positionX;
				var newY = newY+piece.positionY;

				piece.boxes[i][0] = newX;
				piece.boxes[i][1] = newY;
			}
			
			//check if it's inside the pot
			for (i=0; i<4; i++) {
				if ((-1<piece.boxes[i][0])&&(piece.boxes[i][0]<10)&&(piece.boxes[i][1]>=0)) {
					rotatable = true;
				} else {
					rotatable = false;
					break;
				}
			}
			
			//check if it touches other pieces... 
			if (rotatable) { //...but only if it's still rotatable
				for (i=0; i<4; i++) {
					if (
						(pot[piece.boxes[i][0]][piece.boxes[i][1]] == 0) || 
						(pot[piece.boxes[i][0]][piece.boxes[i][1]] == null)
					) {
						rotatable = true;
					} else {
						rotatable = false;
						break;
					}
				}
			}
			
			//if it was not rotatable get it back
			if (!rotatable) {
				for (i=0; i<4; i++) {
					var x = piece.boxes[i][0];
					var y = piece.boxes[i][1];

					x = x-piece.positionX;
					y = y-piece.positionY;

					//rotate back
					var newX = y;
					var newY = -x;
					piecesCanRotate = true;

					//put new coors back in position
					var newX = newX+piece.positionX;
					var newY = newY+piece.positionY;

					piece.boxes[i][0] = newX;
					piece.boxes[i][1] = newY;
				}
			}
		}
	}
		
	function drawPieceInPot() {
		for (i=0; i<4; i++) {
			pot[piece.boxes[i][0]][piece.boxes[i][1]]=piece.letter;
		}
	}
	
	function grabNextPiece() {
		piece.letter = nextPiece.letter;
		piece.positionX = startPositionX;
		piece.positionY = startPositionY;
		updatePieceBoxesValues(piece.letter);
		updatePiecePosition(piece.positionX, piece.positionY);
		checkGameOver();
				
		//and generate a new next Piece
		nextPiece.letter = generateRandomPiece();
		
		//here we reset the lines erased counter before the next piece appears on the screen
		linesErasedCounter = 0;
	}
	
	function checkGameOver() {
		pieceAppearsOverAnother = false;
		for (i=0; i<4; i++) {
			if (
				(pot[piece.boxes[i][0]][piece.boxes[i][1]] == 0) || 
				(pot[piece.boxes[i][0]][piece.boxes[i][1]] == null)
			) {
				pieceAppearsOverAnother = false;
			} else {
				pieceAppearsOverAnother = true;
				break;
			}
		}
		if (pieceAppearsOverAnother == true) {
			
			gameOver = true;
			//check if the players reached an higher record
			if (gameScore>gameLocalRecord) {
				localStorage.tetrisRecord = gameScore;
				newRecord = true;
			}
			
			//and then game over screen
			clearInterval(gameInterval);
			gameOverScreen();
			
			
		
		}
	}
	
	function checkScoreAndUpdateSpeedAndLevel() {
		if (gameScore >= 0) {
			level = 1;
			gameSpeed = startGameSpeed-speedMaxIncrease*(level-1)/(maxLevel-1);
		}
		
		if (gameScore > 500) {
			level = 2;
			gameSpeed = startGameSpeed-speedMaxIncrease*(level-1)/(maxLevel-1);
		}
		
		if (gameScore > 1000) {
			level = 3;
			gameSpeed = startGameSpeed-speedMaxIncrease*(level-1)/(maxLevel-1);
		}
		
		if (gameScore > 2000) {
			level = 4;
			gameSpeed = startGameSpeed-speedMaxIncrease*(level-1)/(maxLevel-1);
		}
		
		if (gameScore > 4000) {
			level = 5;
			gameSpeed = startGameSpeed-speedMaxIncrease*(level-1)/(maxLevel-1);
		}
		
		if (gameScore > 8000) {
			level = 6;
			gameSpeed = startGameSpeed-speedMaxIncrease*(level-1)/(maxLevel-1);
		}
		
		if (gameScore > 16000) {
			level = 7;
			gameSpeed = startGameSpeed-speedMaxIncrease*(level-1)/(maxLevel-1);
		}
		
		if (gameScore > 32000) {
			level = 8;
			gameSpeed = startGameSpeed-speedMaxIncrease*(level-1)/(maxLevel-1);
		}
		
		if (gameScore > 64000) {
			level = 9;
			gameSpeed = startGameSpeed-speedMaxIncrease*(level-1)/(maxLevel-1);
		}
		
		if (gameScore > 128000) {
			level = 10;
			gameSpeed = startGameSpeed-speedMaxIncrease*(level-1)/(maxLevel-1);
		}		
	}


	//this function pauses the game (gamespeed = 0) once called
	function pauseGame() {
		if ((80 in keysDown) && (!gamePaused)) {
			if (!gamePaused) {
				gameSpeed = 1800000000; //a very sad hack indeed
				gamePaused = true; 		
			}
		}
	}
	
	//and here the function to unpause the game pressing spacebar
	function unPauseGame() {
		if ((32 in keysDown) && (gamePaused)) {
			checkScoreAndUpdateSpeedAndLevel();
			gamePaused = false;
		}
	}
	
	var linesToErase = new Array;
	var voidLine = new Array;
	var linesErasedCounter = 0;
	var pausedWhileEraseCounter = 0;
	
	function checkForFullLines() {
		//empty linesToErase
		for (j=0; j<20; j++) {
			linesToErase[j]=true;
		}
				
		for (j=0; j<20; j++) {
			for (i=0; i<10; i++) {
				var numberOfEmptySquares = 0;
				if (pot[i][j] == 0) {
					numberOfEmptySquares++;
				}
				if (numberOfEmptySquares>0) {
					linesToErase[j]=false;
				}
			}
		}
	}
	
	function howManyFullLines() {
		var howMany = 0;
		for (j=0; j<20; j++) {
			if (linesToErase[j]) {
				howMany = howMany+1;
			}
		}
		return howMany;
	}
	
	var gameStopableForErase = true;
	var pausedWhileErasedCounter = 0;
	
	function areThereFullLines() {
		
		if (gameStopableForErase) {
			//stop the game if there are full lines
			for (j=0; j<20; j++) {
				if (linesToErase[j]==true){
					
					gameSpeed = 1000000;
					console.log(gameSpeed);
					gameStopableForErase = false;
					pausedWhileErasedCounter = 1;
					//we need to prevent the user from moving the piece when the blink is on
					piecesMovableLeftWhileBlink = false; 
					piecesMovableRightWhileBlink = false;
					piecesCanRotateWhileBlink = false;
				}
			}
		}
		
		if (pausedWhileErasedCounter != 0) {
			pausedWhileErasedCounter++;
		}

	}
	
	function colorBlinkAndErase() {
	
		//if there are fulllines stop the game for 1000 millisecs
		if (pausedWhileErasedCounter>blinkHowLong) {
			
			eraseFullLines();
			collapseEmptyLines();
			checkForFullLines();
			updateGameScore();
			pausedWhileErasedCounter = 0;
			gameStopableForErase = true;
			
			//and here we give commands back to the player
			piecesMovableLeftWhileBlink = true; 
			piecesMovableRightWhileBlink = true;
			piecesCanRotateWhileBlink = true;
			checkScoreAndUpdateSpeedAndLevel();
		}
		
		//and while the game is stoped and makes 3 pulses of blink
		if (
			((blinkHowLong/7<pausedWhileErasedCounter) && (pausedWhileErasedCounter<2*blinkHowLong/7)) || 
			((3*blinkHowLong/7<pausedWhileErasedCounter) && (pausedWhileErasedCounter<4*blinkHowLong/7)) || 
			((5*blinkHowLong/7<pausedWhileErasedCounter) && (pausedWhileErasedCounter<6*blinkHowLong/7))
		) {
			redLines = true;
		} else {
			redLines = false;
		}
	}
	
	function eraseFullLines() {
		for (j=0; j<20; j++) {
			if (linesToErase[j]) {
				linesErasedCounter++;
				for (i=0; i<10; i++) {
					pot[i][j] = 0; //erase the line	
				}
			}
		}
	}
	
	function updateGameScore(){
		if (linesErasedCounter == 1) {
			gameScore = gameScore+50;
		}
		
		if (linesErasedCounter == 2) {
			gameScore = gameScore+250;
		}
		
		if (linesErasedCounter == 3) {
			gameScore = gameScore+1000;
		}
		
		if (linesErasedCounter == 4) {
			gameScore = gameScore+4000;
		}
	}
		
	function collapseEmptyLines() {
		for (j=19; j>-1; j--) {
			if (linesToErase[j]) {
				for (y=j; y<20; y++) {
					for (i=0; i<10; i++) {
						pot[i][y]=pot[i][y+1];
					}
				}
				for (i=0; i<10; i++) {
					pot[i][19] = 0;
				}
			}
		}
	}
	
	//let's create a counter variable. An addition of every loop will allow to see how mujch time passed.
	var gravityTimeCounted = 0;
	var playerMoveTimeCounted = 0;
	
	
	//don't confuse these variable with piece_ appendix with the proper ones of each moving method function movableLeft, movableRight, etc.
	//this is a global variable we are going to point at to see if the first piece is moving down fast and not the next already.
	var piecesMovableDownFast = true; 
	var piecesMovableLeft = true; 
	var piecesMovableRight = true;
	var piecesCanRotate = true;
	
	//update: the core of the game where everything moves and is decided
	function update(deltaT) {
		
		//each loop we check if the player is not pressing anymore the keys, so that it's possible to reset the event listener "did he pressed it" values
		if (!(40 in keysDown)) {
			piecesMovableDownFast = true;
		}
		
		if (!(37 in keysDown)) {
			piecesMovableLeft = true;
		}
		
		if (!(39 in keysDown)) {
			piecesMovableRight = true;
		}
		
		if (!(38 in keysDown)) {
			piecesCanRotate = true;
		}
		
		gravityTimeCounted = gravityTimeCounted+deltaT;
		playerMoveTimeCounted = playerMoveTimeCounted+deltaT;
		
		
		//THE MOVE
		//fast down for the players to speed up the falling of the piece
		if (piecesMovableDownFast) {
			if ((40 in keysDown) && (!gamePaused)) {
				gravityTimeCounted=gravityTimeCounted+150/1000;
			}
		}
		
		if (gravityTimeCounted>gameSpeed/1000) {
			gravityTimeCounted = 0;
			moveDownThePiece();
		}
		

		
		if ((piecesMovableLeft)&&(piecesMovableLeftWhileBlink)) {
			if ((37 in keysDown) && (!gamePaused)) {
				movePieceLeft();
				playerMoveTimeCounted = 0;
			}
		}
		
		if ((piecesMovableRight)&&(piecesMovableRightWhileBlink)) {
			if ((39 in keysDown) && (!gamePaused)) {
				movePieceRight();
				playerMoveTimeCounted = 0;
			}
		}
		
		if ((piecesCanRotate)&&(piecesCanRotateWhileBlink)) {
			if ((38 in keysDown) && (!gamePaused)) {
				rotatePiece();
			}
		}
		
		//Only exception in our players gameplay control are the left and right command. We want the pieces to move even if the player is pressing them
		if (playerMoveTimeCounted>startGameSpeed/5000) {
			
				piecesMovableLeft = true;
				piecesMovableRight = true;

		}
		
		//Wanna stop? Back to menu... //uncomment when there is actually a game menu
		if (27 in keysDown) {
			gameMenu();
			clearInterval(gameInterval);
		}
			
		//check if player wants to pause the game
		pauseGame();
		
		//check if player wants to unpause the game
		unPauseGame();

		//check which lines have to be erased
		checkForFullLines(); //updates the linesToErase array where the 20 lines of the pot are classified in true (erase) and false 
		
		//erase full lines
		areThereFullLines();
		colorBlinkAndErase();	
		
		//and erase the Lines Erased Counter for the score update
		linesErasedCounter=0;
		
		
				
	}

	//render objects
	function render() {
		//renders background
		ctx.fillStyle = "#000000";
		ctx.fillRect(0, 0, canvas.width, canvas.height);
		
		//renders pot
		//right wall
		ctx.fillStyle = "#ffffff";
		ctx.fillRect(potX+pixDim*11, potY, pixDim, pixDim*21);
		//left wall
		ctx.fillStyle = "#ffffff";
		ctx.fillRect(potX, potY, pixDim, pixDim*21);
		//bottom
		ctx.fillStyle = "#ffffff";
		ctx.fillRect(potX, potY+pixDim*20, pixDim*12, pixDim);
		
		//render pot content
		for (i=0; i<10; i++) {
			for (j=0; j<20; j++) {
				switch (pot[i][j]) {
					// colors are picked respecting the standard provided by The Tetris Company founded by Pajitnov
					case 0:
						ctx.fillStyle = "black"; //empty box
						break;
					case "I":
						ctx.fillStyle = "cyan"; //I piece
						break;
					case "J":
						ctx.fillStyle = "blue"; //J piece
						break;
					case "L":
						ctx.fillStyle = "orange"; //L piece
						break;
					case "O":
						ctx.fillStyle = "yellow"; //O piece
						break;
					case "S":
						ctx.fillStyle = "green"; //S piece
						break;
					case "T":
						ctx.fillStyle = "purple"; //T piece
						break;
					case "Z":
						ctx.fillStyle = "red"; //Z piece
						break;
					default:
						ctx.fillStyle = "rgba(0, 0, 0, 0)";
						console.log("Careful, while drawing the pot content one square had a non valid color");
				}
				ctx.fillRect(potX+pixDim*(i+1), potY+potHeight-pixDim*(j+1), pixDim, pixDim);
				//the lines are erased because confusing, this code can be used in a second time to put a background design in the pot
				/*
				//this second part puts a gray dot in the center of empty squares and gives a ruler to the player that helps to aim
				if (pot[i][j] == 0) {
					ctx.fillStyle = "#999999";
					ctx.fillRect(potX+pixDim*(i+1)+pixDim/2, potY+potHeight-pixDim*(j+1)+pixDim/2, 1, 1);
				}
				*/
			}
		}
		
		//render a square on a certain position
		function renderOneSquareInPot(squareX, squareY, squareColor) { //careful positions in pot are from cartesian 0,0 in the bottom left
			if (
				(0<=squareX) && (squareX<=9) &&
				(0<=squareY) && (squareY<=19)
			) {	
				switch (squareColor) {
					// colors are picked respecting the standard provided by The Tetris Company founded by Pajitnov
					case 0:
						ctx.fillStyle = "black"; //empty box
						break;
					case "I":
						ctx.fillStyle = "cyan"; //I piece
						break;
					case "J":
						ctx.fillStyle = "blue"; //J piece
						break;
					case "L":
						ctx.fillStyle = "orange"; //L piece
						break;
					case "O":
						ctx.fillStyle = "yellow"; //O piece
						break;
					case "S":
						ctx.fillStyle = "green"; //S piece
						break;
					case "T":
						ctx.fillStyle = "purple"; //T piece
						break;
					case "Z":
						ctx.fillStyle = "red"; //Z piece
						break;
					default:
						ctx.fillStyle = "rgba(0, 0, 0, 0)";
						console.log("Careful, one square had a non valid color");
						
				}
				ctx.fillRect(canvas.width/2-pixDim*6+pixDim+squareX*pixDim, canvas.height/16+pixDim*19-squareY*pixDim , pixDim, pixDim);
			} else {
				console.log("Careful, the game is trying to render a square out of the pot");
			}
		}
		
		for (i=0; i<4; i++) {
			renderOneSquareInPot(piece.boxes[i][0], piece.boxes[i][1], piece.letter);
		}
		
		//here the function to make one line be red for the blink effect
		function lineRed(line) {
			ctx.fillStyle = "rgba(100, 100, 100, 0.7)";
			ctx.fillRect(potX+pixDim, potY+19*pixDim-line*pixDim, potWidth, pixDim);
		}
		
		if (redLines) {
			for (j=0; j<20; j++) {
			if (linesToErase[j]) {
				lineRed(j);
			}
		}
		}
		
		
		//and here starts the optional cool staff
		//render Game over
		
		/*
		if (gameOver) {
			ctx.fillStyle = "#ffffff";
			ctx.font = "30px " + gameFont + ", arial, helvetica, sans-serif";
			ctx.textAlign = "center";
			ctx.textBaseline = "center";
			ctx.fillText("Game Over", canvas.width*1/6, canvas.height/2);
		}
		*/
		
		
		//record
		ctx.fillStyle = "#ffffff";
		ctx.font = "30px " + gameFont + ", arial, helvetica, sans-serif";
		ctx.textAlign = "center";
		ctx.textBaseline = "center";
		updateStats();
		ctx.fillText('I : ' + manyIper.toFixed(2) + "%", canvas.width*1/6, canvas.height/4+45*0);
		ctx.fillText('J : ' + manyJper.toFixed(2) + "%", canvas.width*1/6, canvas.height/4+45*1);
		ctx.fillText('L : ' + manyLper.toFixed(2) + "%", canvas.width*1/6, canvas.height/4+45*2);
		ctx.fillText('O : ' + manyOper.toFixed(2) + "%", canvas.width*1/6, canvas.height/4+45*3);
		ctx.fillText('S : ' + manySper.toFixed(2) + "%", canvas.width*1/6, canvas.height/4+45*4);
		ctx.fillText('T : ' + manyTper.toFixed(2) + "%", canvas.width*1/6, canvas.height/4+45*5);
		ctx.fillText('Z : ' + manyZper.toFixed(2) + "%", canvas.width*1/6, canvas.height/4+45*6);
		
		//render level
		ctx.fillStyle = "#ffffff";
		ctx.font = "30px " + gameFont + ", arial, helvetica, sans-serif";
		ctx.textAlign = "center";
		ctx.textBaseline = "center";
		ctx.fillText("Level: " + level, canvas.width*5/6, canvas.height/16+20);
		
		//record display the highest record reached playing on this device
		ctx.fillStyle = "#ffffff";
		ctx.font = "30px " + gameFont + ", arial, helvetica, sans-serif";
		ctx.textAlign = "center";
		ctx.textBaseline = "center";
		ctx.fillText("Record:", canvas.width*5/6, canvas.height/4+20);
		ctx.fillText(gameLocalRecord, canvas.width*5/6, canvas.height/4+45+20);
		
		//Your Score
		ctx.fillStyle = "#ffffff";
		ctx.font = "30px " + gameFont + ", arial, helvetica, sans-serif";
		ctx.textAlign = "center";
		ctx.textBaseline = "center";
		ctx.fillText("Your Score:", canvas.width*5/6, canvas.height/2+20);
		ctx.fillText(gameScore, canvas.width*5/6, canvas.height/2+45+20);
		
		//render next piece
		var nextPieceRenderX = canvas.width*5/6;
		var nextPieceRenderY = canvas.height*15/16-4*pixDim+20;
		
		ctx.fillStyle = "#ffffff";
		ctx.font = "30px " + gameFont + ", arial, helvetica, sans-serif";
		ctx.textAlign = "center";
		ctx.textBaseline = "center";
		ctx.fillText("Next Piece:", nextPieceRenderX, nextPieceRenderY-20);
		
		switch (nextPiece.letter) {
			case "I":
				ctx.fillStyle = "cyan"; //I piece
				ctx.fillRect(nextPieceRenderX-pixDim-pixDim/2, nextPieceRenderY, pixDim*4, pixDim);
				break;
			case "J":
				ctx.fillStyle = "blue"; //J piece
				ctx.fillRect(nextPieceRenderX-pixDim-pixDim/2, nextPieceRenderY, pixDim*3, pixDim);
				ctx.fillRect(nextPieceRenderX+pixDim*2-pixDim-pixDim/2, nextPieceRenderY, pixDim, pixDim*2);
				break;
			case "L":
				ctx.fillStyle = "orange"; //L piece
				ctx.fillRect(nextPieceRenderX-pixDim-pixDim/2, nextPieceRenderY, pixDim*3, pixDim);
				ctx.fillRect(nextPieceRenderX-pixDim-pixDim/2, nextPieceRenderY, pixDim, pixDim*2);
				break;
			case "O":
				ctx.fillStyle = "yellow"; //O piece
				ctx.fillRect(nextPieceRenderX-pixDim/2, nextPieceRenderY, pixDim*2, pixDim*2);
				break;
			case "S":
				ctx.fillStyle = "green"; //S piece
				ctx.fillRect(nextPieceRenderX-pixDim/2, nextPieceRenderY, pixDim*2, pixDim);
				ctx.fillRect(nextPieceRenderX-pixDim/2, nextPieceRenderY, pixDim, pixDim*2);
				ctx.fillRect(nextPieceRenderX-pixDim-pixDim/2, nextPieceRenderY+pixDim, pixDim*2, pixDim);
				break;
			case "T":
				ctx.fillStyle = "purple"; //T piece
				ctx.fillRect(nextPieceRenderX-pixDim-pixDim/2, nextPieceRenderY, pixDim*3, pixDim);
				ctx.fillRect(nextPieceRenderX-pixDim/2, nextPieceRenderY, pixDim, pixDim*2);
				break;
			case "Z":
				ctx.fillStyle = "red"; //Z piece
				ctx.fillRect(nextPieceRenderX-pixDim-pixDim/2, nextPieceRenderY, pixDim*2, pixDim);
				ctx.fillRect(nextPieceRenderX+pixDim-pixDim-pixDim/2, nextPieceRenderY, pixDim, pixDim*2);
				ctx.fillRect(nextPieceRenderX+pixDim-pixDim-pixDim/2, nextPieceRenderY+pixDim, pixDim*2, pixDim);
				
				
				break;
			default:
				ctx.fillStyle = "rgba(0, 0, 0, 0)";
				console.log("Careful, one square had a non valid color");
		}
		
		//game paused, press bar to start the game
		if (gamePaused) {
			ctx.fillStyle = "rgba(0, 0, 0, 0.6)";
			ctx.fillRect(0, 0, canvas.width, canvas.height);
			ctx.fillStyle = "#ffffff";
			ctx.strokeSyle = "rgba(0, 0, 0, 0.6)";
			ctx.font = "60px " + gameFont + ", arial, helvetica, sans-serif";
			ctx.textAlign = "center";
			ctx.textBaseline = "center";
			ctx.fillText("Game Paused", canvas.width/2, canvas.height*0.4);
			ctx.strokeText("Game Paused", canvas.width/2, canvas.height*0.4);
			ctx.font = "30px " + gameFont + ", arial, helvetica, sans-serif";
			ctx.fillText("Press Spacebar to continue...", canvas.width/2, canvas.height*0.6+45);
			ctx.strokeText("Press Spacebar to continue...", canvas.width/2, canvas.height*0.6+45);
			
		}
	}

	//main game loop
	var main = function () {
		var now = Date.now();
		var delta = now-before;
		update(delta/1000);
		render();
		before=now;
	}

	var before = Date.now();
	var gameInterval = setInterval(main, 1); // 1 = Execute as fast as possible; 1000 = execute every second
}


function gameOverScreen() {
	
	updateMenu = function () {
				
		//if y start the game
		if (89 in keysDown) {
            newGame();
			clearInterval(gameOverInterval);
            return;
        }
		
		//if n go back to main menu
		if (78 in keysDown) {
            gameMenu();
			clearInterval(gameOverInterval);
            return;
        }
		
		
	}
	
	renderMenu = function() {
		
		//renders background: for the menu is white
		ctx.fillStyle = "#000000";
		ctx.fillRect(0, 0, canvas.width, canvas.height);
		
		
		
		//congratulate the player for her/his score
		if (newRecord) {

			ctx.fillStyle = "#ffffff";
			ctx.font = "60px " + gameFont + ", arial, helvetica, sans-serif";
			ctx.textAlign = "center";
			ctx.textBaseline = "center";
			ctx.fillText("Congratulations!!!", canvas.width/2, canvas.height/4);
			ctx.font = "45px " + gameFont + ", arial, helvetica, sans-serif";
			ctx.fillText("You set a new record!", canvas.width/2, canvas.height*5/9-25);
			ctx.fillText(gameScore + " points", canvas.width/2, canvas.height*5/9+25);
		
		} else {
			
			//gameover
			ctx.fillStyle = "#ffffff";
			ctx.font = "80px " + gameFont + ", arial, helvetica, sans-serif";
			ctx.textAlign = "center";
			ctx.textBaseline = "center";
			ctx.fillText("Game Over", canvas.width/2, canvas.height/4);
			
			
			//else just display the game score
			ctx.fillStyle = "#ffffff";
			ctx.font = "45px " + gameFont + ", arial, helvetica, sans-serif";
			ctx.textAlign = "center";
			ctx.textBaseline = "center";
			ctx.fillText("Your Score: " + gameScore, canvas.width/2, canvas.height*5/9);
			
		}
		
		
		
		//ask the player if he wants another match
		ctx.fillStyle = "#0ffffff";
		ctx.font = "35px " + gameFont + ", arial, helvetica, sans-serif";
		ctx.textAlign = "center";
		ctx.textBaseline = "center";
		ctx.fillText("Another game?", canvas.width/2, canvas.height*4/5);
		ctx.font = "40px " + gameFont + ", arial, helvetica, sans-serif";
		ctx.fillText("Y / N", canvas.width/2, canvas.height*4/5+55);
	}

	
	//main game loop
	var mainMenu = function () {
		var now = Date.now();
		var delta = now-before;
		updateMenu();
		renderMenu();

		before=now;
	}

	var before = Date.now();
	var gameOverInterval = setInterval(mainMenu, 1); // 1 = Execute as fast as possible; 1000 = execute every second

}


function gameMenu() {
	
	function updateMenu() {
		if (32 in keysDown) {
            clearInterval(menuInterval);
            newGame();
            return;
        }
	}
	
	function renderMenu() {
		
		//renders background: for the menu is white
		ctx.fillStyle = "#000000";
		ctx.fillRect(0, 0, canvas.width, canvas.height);
		
		//tetris
		ctx.fillStyle = "#ffffff";
		ctx.font = "100px " + gameFont + ", arial, helvetica, sans-serif";
		ctx.textAlign = "center";
		ctx.textBaseline = "center";
		ctx.fillText("Tetris", canvas.width/2, canvas.height/4);
		
		//A tribute to Alexey Pajitnov
		ctx.fillStyle = "#ffffff";
		ctx.font = "15px " + gameFont + ", arial, helvetica, sans-serif";
		ctx.textAlign = "center";
		ctx.textBaseline = "center";
		ctx.fillText("A tribute to Alexey Pajitnov", canvas.width*4/7, canvas.height*2/7);
		
		//Controls
		ctx.fillStyle = "#ffffff";
		ctx.font = "40px " + gameFont + ", arial, helvetica, sans-serif";
		ctx.textAlign = "center";
		ctx.textBaseline = "center";
		ctx.font = "25px " + gameFont + ", arial, helvetica, sans-serif";
		ctx.fillText("[Left Arrow] : Move left", canvas.width/2, canvas.height*0.36+35);
		ctx.fillText("[Right Arrow] : Move right", canvas.width/2, canvas.height*0.36+70);
		ctx.fillText("[Up Arrow] : Rotate", canvas.width/2, canvas.height*0.36+105);
		ctx.fillText("[Down Arrow] : Down Fast", canvas.width/2, canvas.height*0.36+140);
		ctx.fillText("[P] : Pause the game", canvas.width/2, canvas.height*0.36+175);	
				
		//Press Space To Start the game...
		ctx.fillStyle = "#ffffff";
		ctx.font = "25px " + gameFont + ", arial, helvetica, sans-serif";
		ctx.textAlign = "center";
		ctx.textBaseline = "center";
		ctx.fillText("Press Spacebar to start a new game...", canvas.width/2, canvas.height*9/10);
	}

	
	//main game loop
	var mainMenu = function () {
		var now = Date.now();
		var delta = now-before;
		updateMenu();
		renderMenu();
	
		before=now;
	}
	
	var before = Date.now();
	var menuInterval = setInterval(mainMenu, 1); // 1 = Execute as fast as possible; 1000 = execute every second
}


gameMenu();



