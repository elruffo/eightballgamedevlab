// Create the canvas
var canvas = document.createElement("canvas");
var ctx = canvas.getContext("2d");
canvas.width = 854;
canvas.height = 480;
canvas.id = "theCanvas";
document.body.appendChild(canvas);

//Global variables
var startGameSpeed = 500; //CHOOSE THE STARTING GAME SPEED: MILLISECONDS BETWEEN MOVES
var gameSpeed = startGameSpeed;
var level = 1;
var gamePaused = false;
var gameFont = "Stalin";
var gameOver = false; //for debugging purpose only

//and here Global Variables for rendering purpose
var pixDim = canvas.height/24;
var potX = canvas.width/2-pixDim*6; //from the left
var potY = canvas.height/16;//from the top
var potWidth = 10*pixDim;
var potHeight = 20*pixDim;

//where pieces start falling
var startPositionX = 4;
var startPositionY = 19;

//audio
var createSound = function(path){
	var newAudio = document.createElement('audio');
    newAudio.setAttribute('src', path);
    return newAudio;
}
var playSound = function(sound){
    sound.currentTime = 0;
    sound.play();
}
var padHitSound = createSound('audio/pad_hit.wav');
var gameStartSound = createSound('audio/game_start.wav');
var borderHitSound = createSound('audio/border_hit.wav');
var loseOneLifeSound = createSound('audio/goal.wav');
var mainMenuSound = createSound('audio/setting.wav');

//generates a random number in between limits
function random(min, max) {
	return (max-min)*Math.random()+min;
}



//Event listener: record if keys are down
var keysDown = {};

addEventListener("keydown", function (e) {keysDown[e.keyCode] = true;}, false);
addEventListener("keyup", function (e) {delete keysDown[e.keyCode];}, false);


var newGame = function () {
	//music game starts
	//playSound(gameStartSound); //interesting enough if sound.curent time is set to zero on the first loop, shots a DOM exception
		
	//reset level and gamespeed if the game restarts
	level = 1;
	numLinesErased = 0;
	gamespeed = startGameSpeed;
	
	//here globals for the game statistics
	var manyI = 0;
	var manyJ = 0;
	var manyL = 0;
	var manyO = 0;
	var manyS = 0;
	var manyT = 0;
	var manyZ = 0;
	
	var manyTot = 0; 
	var manyIper = 0;
	var manyJper = 0;
	var manyLper = 0;
	var manyOper = 0;
	var manySper = 0;
	var manyTper = 0;
	var manyZper = 0;
	
	function updateStats() {
		manyTot = manyI+manyJ+manyL+manyO+manyS+manyT+manyZ;
		manyIper = manyI/manyTot*100;
		manyJper = manyJ/manyTot*100;
		manyLper = manyL/manyTot*100;
		manyOper = manyO/manyTot*100;
		manySper = manyS/manyTot*100;
		manyTper = manyT/manyTot*100;
		manyZper = manyZ/manyTot*100;	
	}
	

	//it's a random piece generator. Returns a letter I J L O S T Z out of a number between 1..7
	function generateRandomPiece() {
		var pieceNum = Math.floor(Math.random()*7+1);
		var pieceLetter = 0;
		switch (pieceNum) {
			case 1:
				pieceLetter = "I"; //I piece
				manyI++;
				break;
			case 2:
				pieceLetter = "J"; //JavaArray piece
				manyJ++;
				break;
			case 3:
				pieceLetter = "L"; //L piece
				manyL++;
				break;
			case 4:
				pieceLetter = "O"; //O piece
				manyO++;
				break;
			case 5:
				pieceLetter = "S"; //S piece
				manyS++;
				break;
			case 6:
				pieceLetter = "T"; //T piece
				manyT++;
				break;
			case 7:
				pieceLetter = "Z"; //Z piece
				manyZ++;
				break;
			default:
				console.log("Careful, generateRandomPiece() couldn't return an appropiate letter");
		}
		return pieceLetter;
	}
	
	//Create objects and specify its properties
		
	// ***POT***
	//here the pot, the game enviroment, an array that takes note of where the pieces are
	var pot = new Array();
	for (i=0; i<10; i++) { //10 columns, here it works because the loop goes from 0...9
		pot[i] = new Array(19); //and here 20 lines
	}
	//make each box postion in the pot void, equal to 0
	for (i=0; i<10; i++) {
		for (j=0; j<20; j++) {
			pot[i][j] = 0;
		}
	}
		
	// *** Next Piece ***
	var nextPiece = new Object();
	nextPiece.letter = generateRandomPiece();
	
	//here the piece object
	var piece = new Object();
	piece.letter = generateRandomPiece();
	piece.boxes = new Array(3);
	piece.positionX = startPositionX;
	piece.positionY = startPositionY;
	for (i=0; i<4; i++) {
		piece.boxes[i] = new Array(2); 
	}
	
	function updatePieceBoxesValues(letter) {
		switch (letter) {
			// colors are picked respecting the standard provided by The Tetris Company founded by Pajitnov
			case "I":
				piece.boxes[0][0]= -1;
				piece.boxes[0][1]= 0;
				piece.boxes[1][0]= 0;
				piece.boxes[1][1]= 0;
				piece.boxes[2][0]= 1;
				piece.boxes[2][1]= 0;
				piece.boxes[3][0]= 2;
				piece.boxes[3][1]= 0;
				break;
			case "J":
				piece.boxes[0][0]= -1;
				piece.boxes[0][1]= 0;
				piece.boxes[1][0]= 0;
				piece.boxes[1][1]= 0;
				piece.boxes[2][0]= 1;
				piece.boxes[2][1]= 0;
				piece.boxes[3][0]= 1;
				piece.boxes[3][1]= -1;
				break;
			case "L":
				piece.boxes[0][0]= -1;
				piece.boxes[0][1]= -1;
				piece.boxes[1][0]= -1;
				piece.boxes[1][1]= 0;
				piece.boxes[2][0]= 0;
				piece.boxes[2][1]= 0;
				piece.boxes[3][0]= 1;
				piece.boxes[3][1]= 0;
				break;
			case "O":
				piece.boxes[0][0]= 0;
				piece.boxes[0][1]= 0;
				piece.boxes[1][0]= 0;
				piece.boxes[1][1]= -1;
				piece.boxes[2][0]= 1;
				piece.boxes[2][1]= 0;
				piece.boxes[3][0]= 1;
				piece.boxes[3][1]= -1;
				
				break;
			case "S":
				piece.boxes[0][0]= -1;
				piece.boxes[0][1]= -1;
				piece.boxes[1][0]= 0;
				piece.boxes[1][1]= -1;
				piece.boxes[2][0]= 0;
				piece.boxes[2][1]= 0;
				piece.boxes[3][0]= 1;
				piece.boxes[3][1]= 0;
				break;
			case "T":
				piece.boxes[0][0]= -1;
				piece.boxes[0][1]= 0;
				piece.boxes[1][0]= 0;
				piece.boxes[1][1]= 0;
				piece.boxes[2][0]= 1;
				piece.boxes[2][1]= 0;
				piece.boxes[3][0]= 0;
				piece.boxes[3][1]= -1;
				
				break;
			case "Z":
				piece.boxes[0][0]= -1;
				piece.boxes[0][1]= 0;
				piece.boxes[1][0]= 0;
				piece.boxes[1][1]= 0;
				piece.boxes[2][0]= 0;
				piece.boxes[2][1]= -1;
				piece.boxes[3][0]= 1;
				piece.boxes[3][1]= -1;
				
				break;
			default:
				ctx.fillStyle = "rgba(0, 0, 0, 0)";
				console.log("Careful, while asigning piece.boxes default values the letter didn't match");
				
		}
	
	}
	
	updatePieceBoxesValues(piece.letter);
		
	function updatePiecePosition(positionX, positionY) {
		for (i=0; i<4; i++) {
			piece.boxes[i][0] = piece.boxes[i][0]+positionX;
		}
		for (i=0; i<4; i++) {
			piece.boxes[i][1] = piece.boxes[i][1]+positionY;
		}
	}
	
	function updateBackPiecePosition(positionX, positionY) {
		for (i=0; i<4; i++) {
			piece.boxes[i][0] = piece.boxes[i][0]-positionX;
		}
		for (i=0; i<4; i++) {
			piece.boxes[i][1] = piece.boxes[i][1]-positionY;
		}
	}
		
	updatePiecePosition(piece.positionX, piece.positionY);
	
	function rotatePiece() {
		updatePiecePosition(startPositionX,startPositionY);
		
	}
	
	function moveDownThePiece() {		
		var movable = true;
		updateBackPiecePosition(piece.positionX, piece.positionY);
		piece.positionY = piece.positionY-1;
		updatePiecePosition(piece.positionX, piece.positionY);
		//check if the piece is touching one other piece
		for (i=0; i<4; i++) {
			if (pot[piece.boxes[i][0]][piece.boxes[i][1]] == 0) {
				movable = true;
			} else {
				movable = false;
				break;
			}
		}
		//check if the piece reached the bottom and make it not movable
		if (movable) { //but only if it's still movable
			for (i=0; i<4; i++) {
				if (piece.boxes[i][1] >= 0) {
					movable = true;
				} else {
					movable = false;
					break;
				}
			}
		}
		if (!movable) { //if it was not possible to move it...
			updateBackPiecePosition(piece.positionX, piece.positionY);
			piece.positionY = piece.positionY+1; //...get it back to the previous position and...
			updatePiecePosition(piece.positionX, piece.positionY);
			//...draw the piece as permanent in the pot...
			drawPieceInPot();
			//...and grab next piece 
			grabNextPiece();
		}
	}
	
	function movePieceLeft() {
		var movableLeft = true;
		//move left
		//get every box back to default
		for (i=0; i<4; i++) {
			piece.boxes[i][0] = piece.boxes[i][0]-piece.positionX;
		}
		//move position left
		piece.positionX--;
		//update everybox position back to new position
		for (i=0; i<4; i++) {
			piece.boxes[i][0] = piece.boxes[i][0] + piece.positionX;
		}
		//check if it's inside the pot	
		for (i=0; i<4; i++) {
			if (piece.boxes[i][0]>=0) {
				movableLeft = true;
			} else {
				movableLeft = false;
				break;
			}
		}
		if (movableLeft) {
			for (i=0; i<4; i++) {
				if (pot[piece.boxes[i][0]][piece.boxes[i][1]] == 0) {
					movableLeft = true;
				} else {
					movableLeft = false;
					break;
				}
			}
		}
		//if not get it back
		if (!movableLeft) {
			//get every box back to default
			for (i=0; i<4; i++) {
				piece.boxes[i][0] = piece.boxes[i][0]-piece.positionX;
			}
			//move position back
			piece.positionX++;
			//update everybox position back to new position
			for (i=0; i<4; i++) {
				piece.boxes[i][0] = piece.boxes[i][0] + piece.positionX;
			}
		}
	}
	
	function movePieceRight() {
		var movableRight = true;
		//move left
		//get every box back to default
		for (i=0; i<4; i++) {
			piece.boxes[i][0] = piece.boxes[i][0]-piece.positionX;
		}
		//move position right
		piece.positionX++;
		//update everybox position back to new position
		for (i=0; i<4; i++) {
			piece.boxes[i][0] = piece.boxes[i][0] + piece.positionX;
		}
		//check if it's inside the pot	
		for (i=0; i<4; i++) {
			if (piece.boxes[i][0]<10) {
				movableRight = true;
			} else {
				movableRight = false;
				break;
			}
		}
		if (movableRight) {
			for (i=0; i<4; i++) {
				if (pot[piece.boxes[i][0]][piece.boxes[i][1]] == 0) {
					movableRight = true;
				} else {
					movableRight = false;
					break;
				}
			}
		}
		//if not get it back
		if (!movableRight) {
			for (i=0; i<4; i++) {
				piece.boxes[i][0] = piece.boxes[i][0]-piece.positionX;
			}
			//move position right
			piece.positionX--;
			//update everybox position back to new position
			for (i=0; i<4; i++) {
				piece.boxes[i][0] = piece.boxes[i][0] + piece.positionX;
			}
		}
	}
	
	function rotatePiece() {
		var rotatable = true; // :), yes rotatable
		//rotate
		for (i=0; i<4; i++) {
			var x = piece.boxes[i][0];
			var y = piece.boxes[i][1];

			x = x-piece.positionX;
			y = y-piece.positionY;

			//rotate
			var newX = -y;
			var newY = x;

			//put new coors back in position
			var newX = newX+piece.positionX;
			var newY = newY+piece.positionY;

			piece.boxes[i][0] = newX;
			piece.boxes[i][1] = newY;
		}
		
		//check if it's inside the pot
		for (i=0; i<4; i++) {
			if (piece.boxes[i][0]<10) {
				rotatable = true;
			} else {
				rotatable = false;
				break;
			}
		}
		
		//check if it touches other pieces... 
		if (rotatable) { //...but only if it's still rotatable
			for (i=0; i<4; i++) {
				if (pot[piece.boxes[i][0]][piece.boxes[i][1]] == 0) {
					rotatable = true;
				} else {
					rotatable = false;
					break;
				}
			}
		}
		
		//if it was not rotatable get it back
		if (!rotatable) {
			for (i=0; i<4; i++) {
				var x = piece.boxes[i][0];
				var y = piece.boxes[i][1];

				x = x-piece.positionX;
				y = y-piece.positionY;

				//rotate
				var newX = y;
				var newY = -x;

				//put new coors back in position
				var newX = newX+piece.positionX;
				var newY = newY+piece.positionY;

				piece.boxes[i][0] = newX;
				piece.boxes[i][1] = newY;
			}
		}
	}
		
	function drawPieceInPot() {
		for (i=0; i<4; i++) {
			pot[piece.boxes[i][0]][piece.boxes[i][1]]=piece.letter;
		}
	}
	
	function grabNextPiece() {
		piece.letter = nextPiece.letter;
		piece.positionX = startPositionX;
		piece.positionY = startPositionY;
		updatePieceBoxesValues(piece.letter);
		updatePiecePosition(piece.positionX, piece.positionY);
		checkGameOver();
				
		//and generate a new next Piece
		nextPiece.letter = generateRandomPiece();
	}
	
	function checkGameOver() {
		pieceAppearsOverAnother = false;
		for (i=0; i<4; i++) {
			if (pot[piece.boxes[i][0]][piece.boxes[i][1]] == 0) {
				pieceAppearsOverAnother = false;
			} else {
				pieceAppearsOverAnother = true;
				break;
			}
		}
		if (pieceAppearsOverAnother == true) {
			console.log("GAME OVER!!!");
			gameOver = true;
			
		
		}
	}
	
	
	
	/*var bar = new Object();
	bar.speed = gamespeed;
	bar.width = canvas.width/5;
	bar.speed = gamespeed;
	bar.width = canvas.width/5; 
	bar.height = 18;
	bar.x = canvas.width/2-bar.width/2;
	bar.y = canvas.height*9/10;*/
	

	

	
	//function to create the pot
	
	/*var tiles = new Array();
	var i = 0;
	var howManyTilesInARow = 7;
	var distanceBtwTiles = 2;
	var howManyRows = 6;
	
	var tilesWidth = (canvas.width-(howManyTilesInARow-1)*distanceBtwTiles)/howManyTilesInARow;
	var tilesHeight = canvas.width/howManyTilesInARow/3;
	var distanceTilesFromTop = canvas.height/6;
	for (k=0; k<howManyRows; k++) {
		for (j=0; j<howManyTilesInARow; j++) {
			tiles[i] = new tileConstructor(j*(tilesWidth+distanceBtwTiles), distanceTilesFromTop+k*(tilesHeight+distanceBtwTiles), tilesWidth, tilesHeight, true);
			i++;
		}
	}*/
	
	//function that empties the pot
	/*function tileConstructor(x, y, width, height, existboolean) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.exist = existboolean;
	}*/

	//this function pauses the game (gamespeed = 0) once called
	function pauseGame() {
		if ((80 in keysDown) && (!gamePaused)) {
			if (!gamePaused) {
				gameSpeed = 1800000000; //a very sad hack indeed
				gamePaused = true; 		
			}
		}
	}
	
	//and here the function to unpause the game pressing spacebar
	function unPauseGame() {
		if ((32 in keysDown) && (gamePaused)) {
			gameSpeed = startGameSpeed;
			gamePaused = false;
		}
	}
	
	function checkForFullLines() {
		var lineIsFull = true;
		var whichLine = 0;
		do {
		//interrupts loop if there is no full line
		for (i=0; i<10; i++) {
			if (pot[i][whichLine] == 0) {
				lineIsFull = false;
				break;
			}
		}
		
		console.log(lineIsFull); //but if there is a full line it erases it
		if (lineIsFull) {
			for (i=0; i<10; i++) {
				numLinesErased++;
				pot[i][whichLine] = 0;
			}
			//and moves downward all the upper squares
			for (j=whichLine; j<20; j++) {
				for (i=0; i<10; i++) {
					pot[i][j] = pot[i][j+1]; 
				}
			}
			// fill in the top line of the pot that now is void
			for (i=0; i<10; i++) {
				pot[i][19] = 0;
			}
		}
			
		//and so on for every line
		whichLine=whichLine+1;
		lineIsFull = true;
		} while ((whichLine<20)&&(lineIsFull))
	}
	
	//let's create a counter variable. An addition of every loop will allow to see how mujch time passed.
	var gravityTimeCounted = 0;
	var playerMoveTimeCounted = 0;
	
	//update: the core of the game where everything moves and is decided
	function update(deltaT) {
		
		gravityTimeCounted = gravityTimeCounted+deltaT;
		playerMoveTimeCounted = playerMoveTimeCounted+deltaT;
		
		//THE MOVE
		//fast down for the players to speed up the falling of the piece
		if ((40 in keysDown) && (!gamePaused)) {
			gravityTimeCounted=gravityTimeCounted+150/1000;
		} 
		
		if (gravityTimeCounted>gameSpeed/1000) {
			gravityTimeCounted = 0;
			moveDownThePiece();
		}
				
		//Player comands in very weird loop that prevents the move to be to sudden
		if (playerMoveTimeCounted>startGameSpeed/6000) {
			playerMoveTimeCounted = 0;
			//move left key37
			if ((37 in keysDown) && (!gamePaused)) {movePieceLeft()};
			//move right key39
			if ((39 in keysDown) && (!gamePaused)) {movePieceRight()};
			//if the player wants to put the piece down fast with down bottom
			if ((38 in keysDown) && (!gamePaused)) {rotatePiece()};
			
		}
		
		//Wanna stop? Back to menu... //uncomment when there is actually a game menu
		/*if (27 in keysDown) {
			playSound(mainMenuSound);
			gameMenu();
			clearInterval(gameInterval);
		}*/ 
			
		//check if player wants to pause the game
		pauseGame();
		
		//check if player wants to unpause the game
		unPauseGame();

		//check if the line is only colorDepth
		checkForFullLines();
	}

	//render objects
	function render() {
		//renders background
		ctx.fillStyle = "#000000";
		ctx.fillRect(0, 0, canvas.width, canvas.height);
		
		//renders pot
		//right wall
		ctx.fillStyle = "#ffffff";
		ctx.fillRect(canvas.width/2+pixDim*5, canvas.height/16, pixDim, pixDim*21);
		//left wall
		ctx.fillStyle = "#ffffff";
		ctx.fillRect(canvas.width/2-pixDim*6, canvas.height/16, pixDim, pixDim*21);
		//bottom
		ctx.fillStyle = "#ffffff";
		ctx.fillRect(canvas.width/2-pixDim*6, canvas.height/16+pixDim*20, pixDim*12, pixDim);
		
		//render pot content
		for (i=0; i<10; i++) {
			for (j=0; j<20; j++) {
				switch (pot[i][j]) {
					// colors are picked respecting the standard provided by The Tetris Company founded by Pajitnov
					case 0:
						ctx.fillStyle = "black"; //empty box
						break;
					case "I":
						ctx.fillStyle = "cyan"; //I piece
						break;
					case "J":
						ctx.fillStyle = "blue"; //J piece
						break;
					case "L":
						ctx.fillStyle = "orange"; //L piece
						break;
					case "O":
						ctx.fillStyle = "yellow"; //O piece
						break;
					case "S":
						ctx.fillStyle = "green"; //S piece
						break;
					case "T":
						ctx.fillStyle = "purple"; //T piece
						break;
					case "Z":
						ctx.fillStyle = "red"; //Z piece
						break;
					default:
						ctx.fillStyle = "rgba(0, 0, 0, 0)";
						console.log("Careful, while drawing the pot content one square had a non valid color");
				}
				ctx.fillRect(potX+pixDim*(i+1), potY+potHeight-pixDim*(j+1), pixDim, pixDim);
			}
		} 
		
		//render a square on a certain position
		function renderOneSquareInPot(squareX, squareY, squareColor) { //careful positions in pot are from cartesian 0,0 in the bottom left
			if (
				(0<=squareX) && (squareX<=9) &&
				(0<=squareY) && (squareY<=19)
			) {	
				switch (squareColor) {
					// colors are picked respecting the standard provided by The Tetris Company founded by Pajitnov
					case 0:
						ctx.fillStyle = "black"; //empty box
						break;
					case "I":
						ctx.fillStyle = "cyan"; //I piece
						break;
					case "J":
						ctx.fillStyle = "blue"; //J piece
						break;
					case "L":
						ctx.fillStyle = "orange"; //L piece
						break;
					case "O":
						ctx.fillStyle = "yellow"; //O piece
						break;
					case "S":
						ctx.fillStyle = "green"; //S piece
						break;
					case "T":
						ctx.fillStyle = "purple"; //T piece
						break;
					case "Z":
						ctx.fillStyle = "red"; //Z piece
						break;
					default:
						ctx.fillStyle = "rgba(0, 0, 0, 0)";
						console.log("Careful, one square had a non valid color");
						
				}
				ctx.fillRect(canvas.width/2-pixDim*6+pixDim+squareX*pixDim, canvas.height/16+pixDim*19-squareY*pixDim , pixDim, pixDim);
			} else {
				console.log("Careful, the game is trying to render a square out of the pot");
			}
		}
		
		for (i=0; i<4; i++) {
			renderOneSquareInPot(piece.boxes[i][0], piece.boxes[i][1], piece.letter);
		}
		
		
		//and here starts the optional cool staff
		//render Game over
		/*
		if (gameOver) {
			ctx.fillStyle = "#ffffff";
			ctx.font = "30px " + gameFont + ", arial, helvetica, sans-serif";
			ctx.textAlign = "center";
			ctx.textBaseline = "center";
			ctx.fillText("Game Over", canvas.width*1/6, canvas.height/2);
		}
		*/
		
		//record
		ctx.fillStyle = "#ffffff";
		ctx.font = "20px " + gameFont + ", arial, helvetica, sans-serif";
		ctx.textAlign = "center";
		ctx.textBaseline = "center";
		updateStats();
		ctx.fillText('"I"s : ' + manyIper.toFixed(2) + "%", canvas.width*1/6, canvas.height/4+45*0);
		ctx.fillText('"J"s : ' + manyJper.toFixed(2) + "%", canvas.width*1/6, canvas.height/4+45*1);
		ctx.fillText('"L"s : ' + manyLper.toFixed(2) + "%", canvas.width*1/6, canvas.height/4+45*2);
		ctx.fillText('"O"s : ' + manyOper.toFixed(2) + "%", canvas.width*1/6, canvas.height/4+45*3);
		ctx.fillText('"S"s : ' + manySper.toFixed(2) + "%", canvas.width*1/6, canvas.height/4+45*4);
		ctx.fillText('"T"s : ' + manyTper.toFixed(2) + "%", canvas.width*1/6, canvas.height/4+45*5);
		ctx.fillText('"Z"s : ' + manyZper.toFixed(2) + "%", canvas.width*1/6, canvas.height/4+45*6);
		
		//render level
		ctx.fillStyle = "#ffffff";
		ctx.font = "20px " + gameFont + ", arial, helvetica, sans-serif";
		ctx.textAlign = "center";
		ctx.textBaseline = "center";
		ctx.fillText("Level: " + level, canvas.width*5/6, canvas.height/16+20);
		
		//record
		ctx.fillStyle = "#ffffff";
		ctx.font = "20px " + gameFont + ", arial, helvetica, sans-serif";
		ctx.textAlign = "center";
		ctx.textBaseline = "center";
		ctx.fillText("Record:", canvas.width*5/6, canvas.height/4);
		ctx.fillText("100000", canvas.width*5/6, canvas.height/4+45);
		
		//Your Score
		ctx.fillStyle = "#ffffff";
		ctx.font = "20px " + gameFont + ", arial, helvetica, sans-serif";
		ctx.textAlign = "center";
		ctx.textBaseline = "center";
		ctx.fillText("Your Score:", canvas.width*5/6, canvas.height/2);
		ctx.fillText(numLinesErased*10, canvas.width*5/6, canvas.height/2+45);
		
		//render next piece
		var nextPieceRenderX = canvas.width*5/6;
		var nextPieceRenderY = canvas.height*15/16-4*pixDim;
		
		ctx.fillStyle = "#ffffff";
		ctx.font = "15px " + gameFont + ", arial, helvetica, sans-serif";
		ctx.textAlign = "center";
		ctx.textBaseline = "center";
		ctx.fillText("Next Piece:", nextPieceRenderX, nextPieceRenderY-20);
		
		switch (nextPiece.letter) {
			case "I":
				ctx.fillStyle = "cyan"; //I piece
				ctx.fillRect(nextPieceRenderX, nextPieceRenderY, pixDim, pixDim*4);
				break;
			case "J":
				ctx.fillStyle = "blue"; //J piece
				ctx.fillRect(nextPieceRenderX, nextPieceRenderY, pixDim, pixDim*3);
				ctx.fillRect(nextPieceRenderX-pixDim, nextPieceRenderY+pixDim*2, pixDim*2, pixDim);
				break;
			case "L":
				ctx.fillStyle = "orange"; //L piece
				ctx.fillRect(nextPieceRenderX, nextPieceRenderY, pixDim, pixDim*3);
				ctx.fillRect(nextPieceRenderX, nextPieceRenderY+pixDim*2, pixDim*2, pixDim);
				break;
			case "O":
				ctx.fillStyle = "yellow"; //O piece
				ctx.fillRect(nextPieceRenderX, nextPieceRenderY, pixDim*2, pixDim*2);
				break;
			case "S":
				ctx.fillStyle = "green"; //S piece
				ctx.fillRect(nextPieceRenderX, nextPieceRenderY, pixDim*2, pixDim);
				ctx.fillRect(nextPieceRenderX, nextPieceRenderY, pixDim, pixDim*2);
				ctx.fillRect(nextPieceRenderX-pixDim, nextPieceRenderY+pixDim, pixDim*2, pixDim);
				break;
			case "T":
				ctx.fillStyle = "purple"; //T piece
				ctx.fillRect(nextPieceRenderX-pixDim, nextPieceRenderY, pixDim*3, pixDim);
				ctx.fillRect(nextPieceRenderX, nextPieceRenderY, pixDim, pixDim*2);
				break;
			case "Z":
				ctx.fillStyle = "red"; //Z piece
				ctx.fillRect(nextPieceRenderX, nextPieceRenderY, pixDim*2, pixDim);
				ctx.fillRect(nextPieceRenderX+pixDim, nextPieceRenderY, pixDim, pixDim*2);
				ctx.fillRect(nextPieceRenderX+pixDim, nextPieceRenderY+pixDim, pixDim*2, pixDim);
				
				
				break;
			default:
				ctx.fillStyle = "rgba(0, 0, 0, 0)";
				console.log("Careful, one square had a non valid color");
		}
		
		
		/*
		
		ctx.fillStyle = "#ffffff";
		ctx.font = "30px " + gameFont + ", arial, helvetica, sans-serif";
		ctx.textAlign = "center";
		ctx.textBaseline = "center";
		ctx.fillText("Level: " + level, canvas.width*5/6, canvas.height/8);
		*/
		
		
		
		//render Help
		
		//render stats
				
		//game paused, press bar to start the game
		if (gamePaused) {
			ctx.fillStyle = "#000000";
			ctx.strokeStyle = "#ffffff";
			ctx.font = "35px " + gameFont + ", arial, helvetica, sans-serif";
			ctx.textAlign = "center";
			ctx.textBaseline = "center";
			ctx.fillText("Game Paused", canvas.width/2, canvas.height/2);
			ctx.strokeText("Game Paused", canvas.width/2, canvas.height/2);
			ctx.font = "25px " + gameFont + ", arial, helvetica, sans-serif";
			ctx.fillText("Press Spacebar to unpause", canvas.width/2, canvas.height/2+45);
			ctx.strokeText("Press Spacebar to unpause", canvas.width/2, canvas.height/2+45);
		}
	}

	//main game loop
	var main = function () {
		var now = Date.now();
		var delta = now-before;
		update(delta/1000);
		render();
		before=now;
	}

	var before = Date.now();
	var gameInterval = setInterval(main, 1); // 1 = Execute as fast as possible; 1000 = execute every second
}



newGame();



