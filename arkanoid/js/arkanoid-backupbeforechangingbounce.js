// Create the canvas
var canvas = document.createElement("canvas");
var ctx = canvas.getContext("2d");
canvas.width = 500;
canvas.height = 600;
canvas.id = "theCanvas";
document.body.appendChild(canvas);

//Global variables
var piGrec=Math.PI;
var startGameSpeed = 200; //CHOOSE THE STARTING GAME SPEED
var gamespeed = startGameSpeed;
var gameAngle = 0.5; //in rad
var gamePaused = false;
var gameFont = "Squid"

//generates a random number in between limits
function random(min, max) {
	return (max-min)*Math.random()+min;
}

//Event listener: record if keys are down
var keysDown = {};

addEventListener("keydown", function (e) {keysDown[e.keyCode] = true;}, false);
addEventListener("keyup", function (e) {delete keysDown[e.keyCode];}, false);


var newGame = function () {

	//bouce on bar function
	function bounceOnBar(objectX, objectY, objectWidth, objectHeight) {
		//1: top border
		if (
			(ball.x<objectX+objectWidth) &&
			(ball.x+ball.width>objectX) &&
			(objectY<ball.y+ball.width) && (ball.y+ball.width<objectY+objectHeight) &&
			(ball.y<objectY)
			) {
				ball.y=objectY-ball.width;
				ball.angle = 2*piGrec - ball.angle;
				updateSpeeds();	
		}
		//2: bottom border
		if (
			(ball.x<objectX+objectWidth) &&
			(ball.x+ball.width>objectX) &&
			(ball.y<objectY+objectHeight) && (ball.y>objectY) &&
			(ball.y+ball.width>objectY+objectHeight)
			) {
				ball.y=objectY+objectHeight;
				ball.angle = 2*piGrec - ball.angle;
				updateSpeeds();	
		}
		//3:left border
		if (
			(ball.y<objectY+objectHeight) &&
			(ball.y+ball.width>objectY) &&
			(ball.x+ball.width>objectX) && (ball.x+ball.width<objectX+objectWidth) &&
			(ball.x<objectX)
			) {
				ball.x=objectX-ball.width;
				ball.angle = piGrec - ball.angle;
				updateSpeeds();	
		}
		//4: right border
		if (
			(ball.y<objectY+objectHeight) &&
			(ball.y+ball.width>objectY) &&
			(objectX<ball.x) && (ball.x<objectX+objectWidth) &&
			(objectX+objectWidth<ball.x+ball.width)
			) {
				ball.x=objectX+objectWidth;
				ball.angle = piGrec - ball.angle;
				updateSpeeds();	
		}
	}

	//bouce on tile function
	function bounceOnTile(objectX, objectY, objectWidth, objectHeight, objectExists) {
		if (objectExists) {
			//1: top border
			if (
				(ball.x<objectX+objectWidth) &&
				(ball.x+ball.width>objectX) &&
				(ball.y+ball.width>objectY) && (ball.y+ball.width<objectY+objectHeight) &&
				(ball.y<objectY)
				) {
					ball.y=objectY-ball.width;
					ball.angle = 2*piGrec - ball.angle;
					updateSpeeds();
					tiles[i].exist = false;
			}
			//2: bottom border
			if (
				(ball.x<objectX+objectWidth) &&
				(ball.x+ball.width>objectX) &&
				(ball.y<objectY+objectHeight) && (ball.y>objectY) &&
				(ball.y+ball.width>objectY+objectHeight)
				) {
					ball.y=objectY+objectHeight;
					ball.angle = 2*piGrec - ball.angle;
					updateSpeeds();	
					tiles[i].exist = false;
			}
			//3:left border
			if (
				(ball.y<objectY+objectHeight) &&
				(ball.y+ball.width>objectY) &&
				(ball.x+ball.width>objectX) && (ball.x+ball.width<objectX+objectWidth) &&
				(ball.x<objectX)
				) {
					ball.x=objectX-ball.width;
					ball.angle = piGrec - ball.angle;
					updateSpeeds();
					tiles[i].exist = false;			
			}
			//4: right border
			if (
				(ball.y<objectY+objectHeight) &&
				(ball.y+ball.width>objectY) &&
				(objectX<ball.x) && (ball.x<objectX+objectWidth) &&
				(objectX+objectWidth<ball.x+ball.width)
				) {
					ball.x=objectX+objectWidth;
					ball.angle = piGrec - ball.angle;
					updateSpeeds();
					tiles[i].exist = false;
			}
		}
	}
		
	//set lives left for 3 at the beginning	
	var lives = 3;
	
	//Create ball and specify its properties
	var ball = new Object();
	ball.width = 15;
	ball.x = canvas.width/2;
	ball.y = canvas.height/2;
	ball.angle = 1;
	ball.speed = gamespeed;
	ball.speedX = ball.speed*Math.cos(ball.angle);
	ball.speedY = ball.speed*Math.sin(ball.angle);


	//Create bar and specify its properties
	var bar = new Object();
	bar.speed = gamespeed;
	bar.width = canvas.width/5;
	bar.speed = gamespeed;
	bar.width = canvas.width/5; 
	bar.height = 30;
	bar.x = canvas.width/2-bar.width/2;
	bar.y = canvas.height*9/10;


	//function to create array
	var tiles = new Array();
	var i = 0;
	var howManyTilesInARow = 10;
	var howManyRows = 5;
	var distanceTilesFromTop = canvas.height/6;
	for (k=0; k<howManyRows; k++) {
		for (j=0; j<howManyTilesInARow; j++) {
			tiles[i] = new tileConstructor(canvas.width*j/howManyTilesInARow, canvas.height/6+k*canvas.width/howManyTilesInARow/2, canvas.width/howManyTilesInARow, canvas.width/howManyTilesInARow/2, true);
			i++;
		}
	}
	
	//Here go the game objects ball, bar and tiles [FIX IT LIKE BAR!!!]
	function drawHeart(x, y, heartWidth) { //pos x, pos y, pixel resolution
		var pixres = heartWidth/7;
		ctx.fillStyle = "#ffffff";
		ctx.fillRect(x+1*pixres, y+1*pixres, 5*pixres, 3*pixres); //#1
		ctx.fillRect(x+0*pixres, y+1*pixres, 7*pixres, 2*pixres); //#2
		ctx.fillRect(x+1*pixres, y+0*pixres, 2*pixres, 4*pixres); //#3
		ctx.fillRect(x+4*pixres, y+0*pixres, 2*pixres, 4*pixres); //#4
		ctx.fillRect(x+2*pixres, y+1*pixres, 3*pixres, 4*pixres); //#5
		ctx.fillRect(x+3*pixres, y+1*pixres, 1*pixres, 5*pixres); //#6
	}

	//function that creates a tile Object
	function tileConstructor(x, y, width, height, existboolean) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.exist = existboolean;
	}


	//this function provides a new random angle to the ball in between limits of choice
	function newAngle() {
		var minAngle = piGrec; //rad
		var maxAngle = 2*piGrec; //rad starts always going down, 360degree would be piGrec/2
		 //between minAngle and maxAngle
		do {
			ball.angle = random(minAngle, maxAngle);
		} while ((ball.angle<piGrec+gameAngle)||(ball.angle>2*piGrec-gameAngle));
	}
	//angle check
	function angleCheck() {
		if (ball.angle>2*piGrec) {
			ball.angle = ball.angle-2*piGrec;
		}
		if (ball.angle<0) {
			ball.angle = 2*piGrec+ball.angle;
		}
	}

	function updateSpeeds() { //updates ball.speedX and ball.speedY, useful to update after angle changes
		angleCheck();
		ball.speedX = gamespeed*Math.cos(ball.angle);
		ball.speedY = gamespeed*Math.sin(ball.angle);
	}
	
	//this function pauses the game (game speed = 0) once called
	function pauseGame() {
		if ((80 in keysDown) && (!gamePaused)) {
			if (!gamePaused) {
				gamespeed = 0;
				updateSpeeds();
				gamePaused = true; 		
			}

		}
	}
	
	//and here the function to unpause the game pressing spacebar
	function unPauseGame() {
		if ((32 in keysDown) && (gamePaused)) {
			gamespeed = startGameSpeed;
			updateSpeeds();
			gamePaused = false;
		}
	}

	newAngle();
	updateSpeeds();

	//update with new bars and ball coordinates
	function update (deltaT) {
	
		//Wanna stop? Back to menu...
		if (27 in keysDown) {
			gameMenu();
			clearInterval(gameInterval);
		}
		
		//check if player wants to pause the game
		pauseGame();
		
		//check if player wants to unpause the game
		unPauseGame();

		//Bar controller: update the bar position
		if ((bar.x>0) && (37 in keysDown)) {bar.x = bar.x - bar.speed*deltaT} //bar goes left
		if ((bar.x<canvas.width-bar.width-1) && (39 in keysDown)) {bar.x = bar.x + bar.speed*deltaT} //bar goes right
			
		//left bounce update the ball angle and position
		if (ball.x<0) {
			ball.x=0;
			ball.angle = piGrec - ball.angle;
			updateSpeeds();	
		}
			
		//right bounce: update the ball angle and position
		if (ball.x>canvas.width-ball.width) {
			ball.x=canvas.width-ball.width;
			ball.angle = piGrec - ball.angle;
			updateSpeeds();	
		}
		
		/*
		//bottom bounce (for testing pourpose only
		if (ball.y>canvas.height-ball.width) {
			ball.y=canvas.height-ball.width;
			ball.angle = 2*piGrec - ball.angle;
			updateSpeeds();	
		}
		*/
		
		
		
		//top bounce (for testing pourpose only
		if (ball.y<0) {
			ball.y=0;
			ball.angle = 2*piGrec - ball.angle;
			updateSpeeds();	
		}
		
		
		//bar bounce: update the ball angle
		bounceOnBar(bar.x, bar.y, bar.width, bar.height);
		
			
		//tiles bounce
		for (i=0; i<tiles.length; i++) {
			bounceOnTile(tiles[i].x, tiles[i].y, tiles[i].width, tiles[i].height, tiles[i].exist);
		}
		
		//Update the ball coordinates 
		ball.x = ball.x + ball.speedX*deltaT;
		ball.y = ball.y + ball.speedY*deltaT;
		
		//lose one life or lose the game
		if (ball.y>canvas.height-ball.width) {
			if (lives != 0) {
				lives = lives-1;
				newAngle();
				updateSpeeds();
				ball.x = canvas.width/2;
				ball.y = canvas.height/2;
			} else {
				gameOver();
				clearInterval(gameInterval);
			}
			
		}

		//victory conditions
		
		//loss condition
		if (ball.y>canvas.height) {
			reset();	
		}
		
		//game over: no more lives left
	}

	//render objects
	function render() {

		//renders background
		ctx.fillStyle = "#000000";
		ctx.fillRect(0, 0, canvas.width, canvas.height);
		
		//Lives left
			
		//Remove comment to monitor ball.angle in game
		//display ball.angle
		ctx.fillStyle = "#ffffff";
		ctx.font = "2em StiffStaff, arial, helvetica, sans-serif";
		ctx.textAlign = "center";
		ctx.textBaseline = "center";
		ctx.fillText(ball.angle, canvas.width/2, canvas.height/2);
		 
		
		//renders ball
		ctx.fillStyle = "#ffffff";
		ctx.fillRect(ball.x, ball.y, ball.width, ball.width);
		
		//render bar
		ctx.fillStyle = "#ffffff";
		ctx.fillRect(bar.x, bar.y, bar.width, bar.height);
		
		//render tiles
		for (i=0; i<tiles.length; i++) {
			if (tiles[i].exist) {
				ctx.fillStyle = "#ffffff";
				ctx.fillRect(tiles[i].x, tiles[i].y, tiles[i].width, tiles[i].height);
			}
		}
		
		
		//draw heart
		if (lives > 0) {
		drawHeart(10, canvas.height-35, 30);
		}
		if (lives > 1) {
		drawHeart(45, canvas.height-35, 30);
		}
		if (lives > 2) {
		drawHeart(80, canvas.height-35, 30);
		}
	}

	//reset the match after the ball falls
	function reset() {
		
		ball.x = canvas.width/2; 
		ball.y = canvas.height/2;
		newAngle();
		updateSpeeds();
		
	}


	//main game loop
	var main = function () {
		var now = Date.now();
		var delta = now-before;
		update(delta/1000);
		render();
		before=now;
	}

	var before = Date.now();
	var gameInterval = setInterval(main, 1); // 1 = Execute as fast as possible; 1000 = execute every second
}

function gameOver() {

	updateMenu = function () {
				
		//if y start the game
		if (89 in keysDown) {
            newGame();
			clearInterval(gameOverInterval);
            return;
        }
		
		//if n go back to main menu
		if (78 in keysDown) {
            gameMenu();
			clearInterval(gameOverInterval);
            return;
        }
		
		
	}
	
	renderMenu = function() {
		
		//renders background: for the menu is white
		ctx.fillStyle = "#ffffff";
		ctx.fillRect(0, 0, canvas.width, canvas.height);
		
		//gameover
		ctx.fillStyle = "#000000";
		ctx.font = "80px " + gameFont + ", arial, helvetica, sans-serif";
		ctx.textAlign = "center";
		ctx.textBaseline = "center";
		ctx.fillText("Game Over", canvas.width/2, canvas.height/4);
		
		//congratulate the player for is score
		
		//ask the player if he wants another match
						
		//Press Space To Start the game...
		ctx.fillStyle = "#000000";
		ctx.font = "25px " + gameFont + ", arial, helvetica, sans-serif";
		ctx.textAlign = "center";
		ctx.textBaseline = "center";
		ctx.fillText("Do you want to play another game? Y / N", canvas.width/2, canvas.height*11/12);
	}

	
	//main game loop
	var mainMenu = function () {
		var now = Date.now();
		var delta = now-before;
		updateMenu();
		renderMenu();

		before=now;
	}

	var before = Date.now();
	var gameOverInterval = setInterval(mainMenu, 1); // 1 = Execute as fast as possible; 1000 = execute every second

}

function gameMenu() {

	updateMenu = function () {
		
		if (32 in keysDown) {
            newGame();
			clearInterval(menuInterval);
            return;
        }
		
		
	}
	
	renderMenu = function() {
		
		//renders background: for the menu is white
		ctx.fillStyle = "#ffffff";
		ctx.fillRect(0, 0, canvas.width, canvas.height);
		
		//PONG
		ctx.fillStyle = "#000000";
		ctx.font = "100px " + gameFont + ", arial, helvetica, sans-serif";
		ctx.textAlign = "center";
		ctx.textBaseline = "center";
		ctx.fillText("Arkanoid", canvas.width/2, canvas.height/4);
						
		//Press Space To Start the game...
		ctx.fillStyle = "#000000";
		ctx.font = "25px " + gameFont + ", arial, helvetica, sans-serif";
		ctx.textAlign = "center";
		ctx.textBaseline = "center";
		ctx.fillText("Press Spacebar to start a new game...", canvas.width/2, canvas.height*11/12);
	}

	
	//main game loop
	var mainMenu = function () {
		var now = Date.now();
		var delta = now-before;
		updateMenu();
		renderMenu();

	
		before=now;
	}

	var before = Date.now();
	var menuInterval = setInterval(mainMenu, 1); // 1 = Execute as fast as possible; 1000 = execute every second

}

gameMenu();



