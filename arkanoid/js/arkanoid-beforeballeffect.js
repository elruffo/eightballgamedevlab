// Create the canvas
var canvas = document.createElement("canvas");
var ctx = canvas.getContext("2d");
canvas.width = 500;
canvas.height = 600;
canvas.id = "theCanvas";
document.body.appendChild(canvas);

//Global variables
var piGrec=Math.PI;
var startGameSpeed = 300; //CHOOSE THE STARTING GAME SPEED
var gamespeed = startGameSpeed;
var level = 1;
var gameAngle = 0.2; //in rad
var gamePaused = false;
var gameFont = "Squid";
var autoPilotOn = false;

//audio
var createSound = function(path){
	var newAudio = document.createElement('audio');
    newAudio.setAttribute('src', path);
    return newAudio;
}
var playSound = function(sound){
    sound.currentTime = 0;
    sound.play();
}
var padHitSound = createSound('audio/pad_hit.wav');
var gameStartSound = createSound('audio/game_start.wav');
var borderHitSound = createSound('audio/border_hit.wav');
var loseOneLifeSound = createSound('audio/goal.wav');
var mainMenuSound = createSound('audio/setting.wav');

//generates a random number in between limits
function random(min, max) {
	return (max-min)*Math.random()+min;
}

//Event listener: record if keys are down
var keysDown = {};

addEventListener("keydown", function (e) {keysDown[e.keyCode] = true;}, false);
addEventListener("keyup", function (e) {delete keysDown[e.keyCode];}, false);


var newGame = function () {
	//music game starts
	playSound(gameStartSound);
	
	
	//reset level if the game restarts
	level = 1;
	gamespeed = startGameSpeed;
			
	//set lives left for 3 at the beginning	
	var lives = 3;
	
	//Create bar and specify its properties
	var bar = new Object();
	bar.speed = gamespeed;
	bar.width = canvas.width/5;
	bar.speed = gamespeed;
	bar.width = canvas.width/5; 
	bar.height = 18;
	bar.x = canvas.width/2-bar.width/2;
	bar.y = canvas.height*9/10;
	
	//Create ball and specify its properties
	var ball = new Object();
	ball.width = 15;
	ball.x = bar.x+bar.width/2-ball.width/2;
	ball.y = bar.y-ball.width-1;
	ball.angle = 1;
	ball.speed = gamespeed;
	ball.speedX = 0;
	ball.speedY = 0;
	//ball.speedX = ball.speed*Math.cos(ball.angle);
	//ball.speedY = ball.speed*Math.sin(ball.angle);
	ball.launched = false;
	
	function ballLauncher() {
		if ((38 in keysDown) && (!gamePaused) && (!ball.launched)) {
			newAngle();
			updateSpeeds();
			ball.launched = true;
		}
	}
	
	//function to create array of tiles
	var tiles = new Array();
	var i = 0;
	var howManyTilesInARow = 7;
	var distanceBtwTiles = 2;
	var howManyRows = 6;
	
	var tilesWidth = (canvas.width-(howManyTilesInARow-1)*distanceBtwTiles)/howManyTilesInARow;
	var tilesHeight = canvas.width/howManyTilesInARow/3;
	var distanceTilesFromTop = canvas.height/6;
	for (k=0; k<howManyRows; k++) {
		for (j=0; j<howManyTilesInARow; j++) {
			tiles[i] = new tileConstructor(j*(tilesWidth+distanceBtwTiles), distanceTilesFromTop+k*(tilesHeight+distanceBtwTiles), tilesWidth, tilesHeight, true);
			i++;
		}
	}
	
	//function that creates a tile Object
	function tileConstructor(x, y, width, height, existboolean) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.exist = existboolean;
	}
	
	//Here go the game objects ball, bar and tiles [FIX IT LIKE BAR!!!]
	function drawHeart(x, y, heartWidth) { //pos x, pos y, pixel resolution
		var pixres = heartWidth/7;
		ctx.fillStyle = "#ffffff";
		ctx.fillRect(x+1*pixres, y+1*pixres, 5*pixres, 3*pixres); //#1
		ctx.fillRect(x+0*pixres, y+1*pixres, 7*pixres, 2*pixres); //#2
		ctx.fillRect(x+1*pixres, y+0*pixres, 2*pixres, 4*pixres); //#3
		ctx.fillRect(x+4*pixres, y+0*pixres, 2*pixres, 4*pixres); //#4
		ctx.fillRect(x+2*pixres, y+1*pixres, 3*pixres, 4*pixres); //#5
		ctx.fillRect(x+3*pixres, y+1*pixres, 1*pixres, 5*pixres); //#6
	}

	//this function provides a new random angle to the ball in between limits of choice
	function newAngle() {
		//for debugging purpose only
		//ball.angle = 2*piGrec-0.52;
		//ball.launched = true;
		
		
		var minAngle = piGrec; //rad
		var maxAngle = 2*piGrec; //rad starts always going down, 360degree would be piGrec/2
		 //between minAngle and maxAngle
		do {
			ball.angle = random(minAngle, maxAngle);
		} while (	
			(ball.angle<piGrec+gameAngle) ||
			((3/2*piGrec-gameAngle<ball.angle) && (ball.angle<3/2*piGrec+gameAngle)) ||
			(ball.angle>2*piGrec-gameAngle)
		);
	}

	//angle check
	function angleCheck() {
		if (ball.angle>2*piGrec) {
			ball.angle = ball.angle-2*piGrec;
		}
		if (ball.angle<0) {
			ball.angle = 2*piGrec+ball.angle;
		}
	}

	//updates ball.speedX and ball.speedY, useful to update after angle changes
	function updateSpeeds() { 
		angleCheck();
		ball.speedX = gamespeed*Math.cos(ball.angle);
		ball.speedY = gamespeed*Math.sin(ball.angle);
	}
	
	//this function pauses the game (gamespeed = 0) once called
	function pauseGame() {
		if ((80 in keysDown) && (!gamePaused)) {
			if (!gamePaused) {
				gamespeed = 0;
				updateSpeeds();
				gamePaused = true; 		
			}
		}
	}
	

	
	//and here the function to unpause the game pressing spacebar
	function unPauseGame() {
		if ((32 in keysDown) && (gamePaused)) {
			gamespeed = startGameSpeed;
			updateSpeeds();
			gamePaused = false;
		}
	}

	
	
	var tilesLeft;
	
	//update with new bars and ball coordinates
	function update (deltaT) {
		
		//bounce on bar function
		function bounceOnSomething(objectX, objectY, objectWidth, objectHeight) {
			//let's create the ball A B C D and bar coordinates points E F G H
			/*
			var pointA = { x: ball.x, y: ball.y }
			var pointB = { x: ball.x+ball.width, y: ball.y  }
			var pointC = { x: ball.x+ball.width, y: ball.y+ball.width }
			var pointD = { x: ball.x, y: ball.y+ball.width }
			var pointE = { x: bar.x, y: bar.y }
			var pointF = { x: bar.x+bar.width, y: bar.y  }
			var pointG = { x: bar.x+bar.width, y: bar.y+bar.height }
			var pointH = { x: bar.x, y: bar.y+bar.height }
			var pointM = { x: bar.x+bar.height/2, y: bar.y+bar.height/2 }
			var pointN = { x: bar.x+bar.width-bar.height/2, y: bar.y+bar.height/2 }
			*/
			
			var pointA = { x: ball.x, y: ball.y }
			var pointB = { x: ball.x+ball.width, y: ball.y  }
			var pointC = { x: ball.x+ball.width, y: ball.y+ball.width }
			var pointD = { x: ball.x, y: ball.y+ball.width }
			var pointE = { x: objectX, y: objectY }
			var pointF = { x: objectX+objectWidth, y: objectY  }
			var pointG = { x: objectX+objectWidth, y: objectY+objectHeight }
			var pointH = { x: objectX, y: objectY+objectHeight }
			var pointM = { x: objectX+objectHeight/2, y: objectY+objectHeight/2 }
			var pointN = { x: objectX+objectWidth-objectHeight/2, y: objectY+objectHeight/2 }
			
			
			//first condition, 2 bottom points of the ball in upper bar part
			if (
				(pointD.x>=pointE.x) &&
				(pointC.x<=pointF.x) && 
				(pointD.y>=pointE.y) && 
				(pointA.y<pointE.y)
			) {
				ball.angle = 2*piGrec - ball.angle;
				updateSpeeds();
				ball.y = objectY - ball.width;
				return true;
			} else if ( //second condition, 2 top points of the ball in lower bar part
				(pointA.x>=pointH.x) &&
				(pointB.x<=pointG.x) && 
				(pointD.y>pointH.y) && 
				(pointA.y<=pointH.y)
			) {
				ball.angle = 2*piGrec - ball.angle;
				updateSpeeds();
				ball.y = objectY + objectHeight;
				return true;
			} else if ( //third condition, 2 top points of the ball in left bar part
				(pointB.y>=pointE.y) &&
				(pointC.y<pointH.y) && 
				(pointA.x<pointH.x) && 
				(pointB.x>=pointH.x)
			) {
				ball.angle = piGrec - ball.angle;
				updateSpeeds();
				ball.x = objectX - ball.width-1;
				return true;
			} else if ( //forth condition, 2 top points of the ball in right bar part
				(pointA.y>=pointF.y) &&
				(pointD.y<pointG.y) && 
				(pointA.x<=pointG.x) && 
				(pointB.x>pointG.x)
			) {
				ball.angle = piGrec - ball.angle;
				updateSpeeds();
				ball.x = objectX + objectWidth +1;
				return true;
			} else if ( //the C angle is inside
				(pointC.x>pointE.x) &&
				(pointC.y>pointE.y) &&
				(pointC.x<pointF.x) &&
				(pointC.y<pointH.y) &&
				(pointB.y<pointE.y) &&
				(pointA.x<pointE.x)
			) {
				if ( pointC.x-pointE.x<pointC.y-pointE.y) { //if point C got inside more orizontally than vertically
					ball.angle = piGrec - ball.angle; //bounce left
					updateSpeeds();
					ball.x = objectX - ball.width-1;
					return true;
				} else {
					ball.angle = 2*piGrec - ball.angle; //else bounce up
					updateSpeeds();
					ball.y = objectY - ball.width;
					return true;
				}
			} else if (
				(pointD.x>pointE.x) &&
				(pointD.y>pointE.y) &&
				(pointD.x<pointF.x) &&
				(pointD.y<pointH.y) &&
				(pointA.y<pointF.y) &&
				(pointB.x>pointF.x)
			) {
				if ( pointF.x-pointD.x<pointD.y-pointF.y ) { //if point D got inside more orizontally than vertically
					ball.angle = piGrec - ball.angle; //bounce right
					updateSpeeds();
					ball.x = objectX + objectWidth +1;
					return true;
				} else {
					ball.angle = 2*piGrec - ball.angle; //else bounce up
					updateSpeeds();
					ball.y = objectY - ball.width;
					return true;
				}		
			} else if ( //if point B is inside
				(pointB.x>=pointH.x) &&
				(pointB.y<pointH.y) &&
				(pointB.x<pointG.x) &&
				(pointB.y>pointE.y) &&
				(pointA.x<pointH.x) &&
				(pointD.y>pointH.y)
			) {
				if ( pointB.x-pointH.x<pointH.y-pointB.y) { //if point B got inside more orizontally than vertically
					
					ball.angle = piGrec - ball.angle; //bounce left
					updateSpeeds();
					ball.x = objectX -ball.width-1;
					return true;
				} else {
					
					ball.angle = 2*piGrec - ball.angle; //else bounce down
					updateSpeeds();
					ball.y = objectY + objectHeight +1;
					return true;
				}
			} else if ( //if point A only is inside
				(pointA.x>pointH.x) &&
				(pointA.x<pointG.x) &&
				(pointA.y>pointF.y) &&
				(pointA.y<pointG.y) &&
				(pointB.x>pointG.x) &&
				(pointC.y>pointG.y)
			) {
				if ( pointG.x-pointA.x<pointG.y-pointA.y) { //if point A got inside more orizontally than vertically
					
					ball.angle = piGrec - ball.angle; //bounce right
					updateSpeeds();
					ball.x = objectX + objectWidth+1;
					return true;
				} else {
					
					ball.angle = 2*piGrec - ball.angle; //else bounce down
					updateSpeeds();
					ball.y = objectY + objectHeight+1;
					return true;
				}
			}
			
			
			


		}
	
		//Wanna stop? Back to menu...
		if (27 in keysDown) {
			playSound(mainMenuSound);
			gameMenu();
			clearInterval(gameInterval);
		}
		
		//launch the ball!
		
		if (!ball.launched) {
			ball.x = bar.x+bar.width/2-ball.width/2;
			ball.y = bar.y-ball.width-1;
			ballLauncher();
		}
		
		//check if player wants to pause the game
		pauseGame();
		
		//check if player wants to unpause the game
		unPauseGame();

		//Bar controller: update the bar position
		if (!autoPilotOn) {
			if ((bar.x>0) && (37 in keysDown) && (!gamePaused)) {bar.x = bar.x - bar.speed*deltaT} //bar goes left
			if ((bar.x<canvas.width-bar.width-1) && (39 in keysDown) && (!gamePaused)) {bar.x = bar.x + bar.speed*deltaT} //bar goes right
		} else {
			bar.x = ball.x;
		
		}	
		//left bounce update the ball angle and position
		if (ball.x<0) {
			ball.x=0;
			ball.angle = piGrec - ball.angle;
			playSound(borderHitSound);
			updateSpeeds();
		}
			
		//right bounce: update the ball angle and position
		if (ball.x>canvas.width-ball.width) {
			ball.x=canvas.width-ball.width;
			ball.angle = piGrec - ball.angle;
			playSound(borderHitSound);
			updateSpeeds();	
		}
		
		
		//bottom bounce (for testing pourpose only)
		/*
		if (ball.y>canvas.height-ball.width) {
			ball.y=canvas.height-ball.width;
			ball.angle = 2*piGrec - ball.angle;
			updateSpeeds();	
		}
		*/
		
		//top bounce (for testing pourpose only
		if (ball.y<0) {
			ball.y=0;
			ball.angle = 2*piGrec - ball.angle;
			playSound(borderHitSound);
			updateSpeeds();	
		}
		
		
		
		//bar bounce: update the ball angle
		var bouncedOnBar = false;
		bouncedOnBar = bounceOnSomething(bar.x, bar.y, bar.width, bar.height);
		if (bouncedOnBar) {
		playSound(padHitSound);
		}
			
		//tiles bounce
		for (i=0; i<tiles.length; i++) {
			var bounced = false;
			if (tiles[i].exist)
			bounced = bounceOnSomething(tiles[i].x, tiles[i].y, tiles[i].width, tiles[i].height);
			if (bounced) {
				tiles[i].exist = false;
				playSound(padHitSound);
				break;
			}
		}
		
		//Update the ball coordinates 
		ball.x = ball.x + ball.speedX*deltaT;
		ball.y = ball.y + ball.speedY*deltaT;
		
		
		//lose one life or lose the game
		if (ball.y>canvas.height-ball.width) {
			if (lives != 0) {
				lives = lives-1;
				ball.launched = false;
				playSound(loseOneLifeSound);
			} else {
				gameOver();
				playSound(loseOneLifeSound); //change it with a game over sound!
				clearInterval(gameInterval);
			}	
		}

		//victory conditions
		function updateTilesLeft() {
			tilesLeft = 0;
			for (k=0; k<tiles.length; k++) {
				if (tiles[k].exist) {
					tilesLeft = tilesLeft+1;
				}
			}
		}
		updateTilesLeft();
		if (tilesLeft==0) {
			level = level+1;
			gamespeed = gamespeed + 100;
			bar.speed = gamespeed;
			for (l=0; l<tiles.length; l++) {
				tiles[l].exist=true;
			}
			ball.launched = false;			
		}
		
		
		
		//loss condition
		if (ball.y>canvas.height) {
			reset();	
		}
	}

	//render objects
	function render() {

		//renders background
		ctx.fillStyle = "#000000";
		ctx.fillRect(0, 0, canvas.width, canvas.height);
		
		/*
		//tiles left
		ctx.fillStyle = "#ffffff";
		ctx.font = "2em StiffStaff, arial, helvetica, sans-serif";
		ctx.textAlign = "center";
		ctx.textBaseline = "center";
		ctx.fillText("Tiles left: " + tilesLeft, canvas.width/2, canvas.height/10);
		*/
		
		//render level
		ctx.fillStyle = "#ffffff";
		ctx.font = "55px " + gameFont + ", arial, helvetica, sans-serif";
		ctx.textAlign = "center";
		ctx.textBaseline = "center";
		ctx.fillText("Level: " + level, canvas.width/2, canvas.height/8);
			
		//Remove comment to monitor ball.angle in game
		//display ball.angle 
		/*
		ctx.fillStyle = "#ffffff";
		ctx.font = "2em " + gameFont + ", arial, helvetica, sans-serif";
		ctx.textAlign = "center";
		ctx.textBaseline = "center";
		ctx.fillText(ball.angle, canvas.width/2, canvas.height/2);
		*/
		 
		
		//renders ball
		ctx.fillStyle = "#ffffff";
		ctx.fillRect(ball.x, ball.y, ball.width, ball.width);
		
		//render bar
		ctx.fillStyle = "#ffffff";
		ctx.fillRect(bar.x, bar.y, bar.width, bar.height);
		
		//render tiles
		for (i=0; i<tiles.length; i++) {
			if (tiles[i].exist) {
				ctx.fillStyle = "#ffffff";
				ctx.fillRect(tiles[i].x, tiles[i].y, tiles[i].width, tiles[i].height);
			}
		}
		
		//game paused, press bar to start the game
		if (gamePaused) {
			ctx.fillStyle = "#ffffff";
			ctx.font = "45px " + gameFont + ", arial, helvetica, sans-serif";
			ctx.textAlign = "center";
			ctx.textBaseline = "center";
			ctx.fillText("Game Paused", canvas.width/2, canvas.height/2);
			ctx.font = "25px " + gameFont + ", arial, helvetica, sans-serif";
			ctx.fillText("Press Spacebar to unpause", canvas.width/2, canvas.height/2+45);
		}
		
		//needs to launch the ball
		if ((!ball.launched) && (!gamePaused)) {
			ctx.fillStyle = "#ffffff";
			ctx.font = "40px " + gameFont + ", arial, helvetica, sans-serif";
			ctx.textAlign = "center";
			ctx.textBaseline = "center";
			ctx.fillText("Press [up arrow]", canvas.width/2, canvas.height/2);
			ctx.font = "33px " + gameFont + ", arial, helvetica, sans-serif";
			ctx.fillText("And launch the ball!", canvas.width/2, canvas.height/2+45);
		}
		
		
		
		
		
		
		//draw heart
		if (lives > 0) {
		drawHeart(10, canvas.height-35, 30);
		}
		if (lives > 1) {
		drawHeart(45, canvas.height-35, 30);
		}
		if (lives > 2) {
		drawHeart(80, canvas.height-35, 30);
		}
	}

	//reset the match after the ball falls
	function reset() {
		
		ball.x = canvas.width/2; 
		ball.y = canvas.height/2;
		newAngle();
		updateSpeeds();
		
	}


	//main game loop
	var main = function () {
		var now = Date.now();
		var delta = now-before;
		update(delta/1000);
		render();
		before=now;
	}

	var before = Date.now();
	var gameInterval = setInterval(main, 1); // 1 = Execute as fast as possible; 1000 = execute every second
}

function gameOver() {
	
	updateMenu = function () {
				
		//if y start the game
		if (89 in keysDown) {
            newGame();
			clearInterval(gameOverInterval);
            return;
        }
		
		//if n go back to main menu
		if (78 in keysDown) {
			playSound(mainMenuSound);
            gameMenu();
			clearInterval(gameOverInterval);
            return;
        }
		
		
	}
	
	renderMenu = function() {
		
		//renders background: for the menu is white
		ctx.fillStyle = "#ffffff";
		ctx.fillRect(0, 0, canvas.width, canvas.height);
		
		//gameover
		ctx.fillStyle = "#000000";
		ctx.font = "80px " + gameFont + ", arial, helvetica, sans-serif";
		ctx.textAlign = "center";
		ctx.textBaseline = "center";
		ctx.fillText("Game Over", canvas.width/2, canvas.height/4);
		
		//congratulate the player for is score
		
		//ask the player if he wants another match
						
		//Press Space To Start the game...
		ctx.fillStyle = "#000000";
		ctx.font = "45px " + gameFont + ", arial, helvetica, sans-serif";
		ctx.textAlign = "center";
		ctx.textBaseline = "center";
		ctx.fillText("Another game?", canvas.width/2, canvas.height*3/5);
		ctx.font = "50px " + gameFont + ", arial, helvetica, sans-serif";
		ctx.fillText("Y / N", canvas.width/2, canvas.height*3/5+55);
	}

	
	//main game loop
	var mainMenu = function () {
		var now = Date.now();
		var delta = now-before;
		updateMenu();
		renderMenu();

		before=now;
	}

	var before = Date.now();
	var gameOverInterval = setInterval(mainMenu, 1); // 1 = Execute as fast as possible; 1000 = execute every second

}

function gameMenu() {
	
	
	
	updateMenu = function () {
		
		if (32 in keysDown) {
            newGame();
			clearInterval(menuInterval);
            return;
        }
		
		
	}
	
	renderMenu = function() {
		
		//renders background: for the menu is white
		ctx.fillStyle = "#ffffff";
		ctx.fillRect(0, 0, canvas.width, canvas.height);
		
		//arkanoid
		ctx.fillStyle = "#000000";
		ctx.font = "100px " + gameFont + ", arial, helvetica, sans-serif";
		ctx.textAlign = "center";
		ctx.textBaseline = "center";
		ctx.fillText("Arkanoid", canvas.width/2, canvas.height/4);
		
		//alpha
		ctx.fillStyle = "#000000";
		ctx.font = "50px " + gameFont + ", arial, helvetica, sans-serif";
		ctx.textAlign = "center";
		ctx.textBaseline = "center";
		ctx.fillText("Alpha", canvas.width*4/5, canvas.height/4+45);
		
		
		//Controls
		ctx.fillStyle = "#000000";
		ctx.font = "40px " + gameFont + ", arial, helvetica, sans-serif";
		ctx.textAlign = "center";
		ctx.textBaseline = "center";
		ctx.fillText("*Controls:*", canvas.width/2, canvas.height/2);
		ctx.font = "30px " + gameFont + ", arial, helvetica, sans-serif";
		ctx.fillText("[Left Arrow]: Move left", canvas.width/2, canvas.height/2+45);
		ctx.fillText("[Right Arrow]: Move right", canvas.width/2, canvas.height/2+90);
		//ctx.font = "20px " + gameFont + ", arial, helvetica, sans-serif";
		ctx.fillText("[P]: Pause the game", canvas.width/2, canvas.height/2+135);
						
		//Press Space To Start the game...
		ctx.fillStyle = "#000000";
		ctx.font = "25px " + gameFont + ", arial, helvetica, sans-serif";
		ctx.textAlign = "center";
		ctx.textBaseline = "center";
		ctx.fillText("Press Spacebar to start a new game...", canvas.width/2, canvas.height*11/12);
	}

	
	//main game loop
	var mainMenu = function () {
		var now = Date.now();
		var delta = now-before;
		updateMenu();
		renderMenu();

	
		before=now;
	}

	var before = Date.now();
	var menuInterval = setInterval(mainMenu, 1); // 1 = Execute as fast as possible; 1000 = execute every second

}

gameMenu();



