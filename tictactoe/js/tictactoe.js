/* Mouse even handler*/

// Create the canvas
var canvas = document.createElement("canvas");
var ctx = canvas.getContext("2d");
canvas.width = 500;
canvas.height = 500;
canvas.id = "theCanvas";
//document.body.setAttribute("style", "background-color: pink");
document.body.appendChild(canvas);

//hide mouse while on canvas
document.getElementById('theCanvas').style.cursor = "none";

var nFlr = function(wcomma) {
	return Math.floor(wcomma)
}


// GLOBALS OUT OF THE LOOP
var fps = 0;

var bIsMatchOver = false;


// GAME OBJECTS
var mouse = {
	posX: 1,
	posY: 1,
	click: false,
}

var col = {
	whiteLight: "#ffffff",
	whiteDark: "#444444",
	black: "#000000",
}

var turn = 1; //1 = cX, 2 = O


//grid [0 = empty, 1 = X, 2 = O]

//create the grid as array of arrays, 1 array for 1 line (sqX, sqY)
var grid = new Array();
for (var i = 0; i<3; i++) {
	grid[i] = new Array;
}
//and set each square to 0, which is empty, and false, which means that regular game color is active
for (var i = 0; i<3; i++) {
	for (var j = 0; j<3; j++) {
		grid[i][j]=new Array;
		grid[i][j][0]=0;
		grid[i][j][1]=false;
	}
}


var updateFps = function(delta) {
	fps = (1000/delta).toFixed(0);
}

// HERE EVENT LISTENERS SECTION

//mouse position event listener updates mouse position
function getMousePos(mycanvas, evt) {
	var rect = mycanvas.getBoundingClientRect();
	mouse.posY= evt.clientY - rect.top;
	mouse.posX= evt.clientX - rect.left; 
}

canvas.addEventListener('mousemove', function(evt) {getMousePos(canvas, evt);}, false);



//here mouse click listener, which adds signs in the grid on proper click

var whichSquare = function() { 
	//carefull, it can return null, handle exception properly!
	var x = null;
	var y = null;
	
	if (
		(2/24*canvas.width<mouse.posX) && 
		(mouse.posX<canvas.width*8/24+canvas.width*1/24/4)
	) {
		x = 0;
	}

	if (
		(2/24*canvas.height<mouse.posY) && 
		(mouse.posY<canvas.height*8/24+canvas.height*1/24/4)
	) {
		y = 0;
	}

	if (
		(2/24*canvas.width+7/24*canvas.width<mouse.posX) && 
		(mouse.posX<canvas.width*8/24+canvas.width*1/24/4+7/24*canvas.width)
	) {
		x = 1;
	}

	if (
		(2/24*canvas.height+7/24*canvas.height<mouse.posY) && 
		(mouse.posY<canvas.height*8/24+canvas.height*1/24/4+7/24*canvas.height)
	) {
		y = 1;
	}

	if (
		(2/24*canvas.width+14/24*canvas.width<mouse.posX) && 
		(mouse.posX<canvas.width*8/24+canvas.width*1/24/4+14/24*canvas.width)
	) {
		x = 2;
	}

	if (
		(2/24*canvas.height+14/24*canvas.height<mouse.posY) && 
		(mouse.posY<canvas.height*8/24+canvas.height*1/24/4+14/24*canvas.height)
	) {
		y = 2;
	}
	
	return [x, y]		
}

var addSign = function() {
	//if the match is not over
	if (!bIsMatchOver) {
		//add cross to the the value returned by whichSquare
		if ((whichSquare()[0]!=null) && (whichSquare()[1]!=null)) { //import: null exception must be handled
			var squareX = whichSquare()[0];
			var squareY = whichSquare()[1];
			//check if the square is empty
			if (grid[squareX][squareY][0]==0) {
				//write X or O based on var turn
				if (turn==1) {
					grid[squareX][squareY][0]=1;
					turn = 2;
				} else {
					grid[squareX][squareY][0]=2;
					turn = 1;
				}
			}
		}
	}	
}

// event listener adds the sign on click
canvas.addEventListener('click', function(evt) {addSign();}, false);



//update game variable based on players action
var update = function(delta) {

	// turn all squares gray
	var allSquaresGray = function() {
		for (var i = 0; i<3; i++) {
			for (var j = 0; j<3; j++) {
				grid[i][j][1]=true;
			}
		}
	}

	// checks if game is over and updates grid colors
	var whereIsTheWin = function() {
		//check the lines 1, 2, 3
		for (var i = 0; i<3; i++) {
			if ((grid[0][i][0] != 0) && (grid[0][i][0] == grid[1][i][0]) && (grid[1][i][0] == grid[2][i][0])) {
				return i+1;
			}
		}

		//check the columns 4, 5, 6
		for (var i = 0; i<3; i++) {
			if ((grid[i][0][0] != 0) && (grid[i][0][0] == grid[i][1][0]) && (grid[i][1][0] == grid[i][2][0])) {
				return i+4;
			}
		}

		//check diagonal topleft bottomright 7
		if ((grid[1][1][0] != 0) && (grid[0][0][0] == grid[1][1][0]) && (grid[1][1][0] == grid[2][2][0])) {
			return 7;
		}

		//check diagonal topright bottomleft 8
		if ((grid[1][1][0] != 0) && (grid[0][2][0] == grid[1][1][0]) && (grid[1][1][0] == grid[2][0][0])) {
			return 8;
		}

		//check if all full
		var allFull = function() {
			for (var i = 0; i<3; i++) { 
				for (var j=0; j<3; j++) {
					if (grid[i][j][0]==0) {
						return false;
					}
				}
			}
			return true;
		}

		if (allFull()) {
			return 10 //all full
		}
			
	}

	

	if ((whereIsTheWin()>0) && (whereIsTheWin()<11)) {
		bIsMatchOver = true;
		allSquaresGray();

		if (whereIsTheWin()==1) { 
			grid[0][0][1] = false;
			grid[1][0][1] = false;
			grid[2][0][1] = false;
		} // first line

		if (whereIsTheWin()==2) { 
			grid[0][1][1] = false;
			grid[1][1][1] = false;
			grid[2][1][1] = false;
		} // second line

		if (whereIsTheWin()==3) { 
			grid[0][2][1] = false;
			grid[1][2][1] = false;
			grid[2][2][1] = false;
		} // third line

		if (whereIsTheWin()==4) { 
			grid[0][0][1] = false;
			grid[0][1][1] = false;
			grid[0][2][1] = false;
		} // first column

		if (whereIsTheWin()==5) { 
			grid[1][0][1] = false;
			grid[1][1][1] = false;
			grid[1][2][1] = false;
		} // second column

		if (whereIsTheWin()==6) { 
			grid[2][0][1] = false;
			grid[2][1][1] = false;
			grid[2][2][1] = false;
		} // third column

		if (whereIsTheWin()==7) { 
			grid[0][0][1] = false;
			grid[1][1][1] = false;
			grid[2][2][1] = false;
		} // topleft bottomright 7

		if (whereIsTheWin()==8) { 
			grid[2][0][1] = false;
			grid[1][1][1] = false;
			grid[0][2][1] = false;
		} // topleft bottomright 8

		if (whereIsTheWin()==10) { 
			
		} // all full 10



	}

	//but if game is over keep the line that won lighten up	
}

// renders the updated game variable
var render = function(positionX, sqY) {


}


//main loop
var mainLoop = function () {
	var now = Date.now();
	var delta = now-before;

	//here the functions to repeat every loop
	update();
	updateFps(delta);
	render();
	
	before=now;
}


var before = Date.now();
var gameInterval = setInterval(mainLoop, 1); //1 = one millisecond. Set as low as possible for fast execution. (every millisec.)