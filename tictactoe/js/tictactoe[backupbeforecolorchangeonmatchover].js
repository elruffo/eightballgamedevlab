/* Mouse even handler*/

// Create the canvas
var canvas = document.createElement("canvas");
var ctx = canvas.getContext("2d");
canvas.width = 500;
canvas.height = 500;
canvas.id = "theCanvas";
//document.body.setAttribute("style", "background-color: pink");
document.body.appendChild(canvas);

//hide mouse while on canvas
document.getElementById('theCanvas').style.cursor = "none";

var nFlr = function(wcomma) {
	return Math.floor(wcomma)
}


// GLOBALS OUT OF THE LOOP
var fps = 0;

var bIsMatchOver = false;


// GAME OBJECTS
var mouse = {
	posX: 1,
	posY: 1,
	click: false,
}

var col = {
	whiteLight: "#ffffff",
	whiteDark: "#444444",
	black: "#000000",
}

var turn = 1; //1 = cX, 2 = O


//grid [0 = empty, 1 = X, 2 = O]

//create the grid as array of arrays, 1 array for 1 line (sqX, sqY)
var grid = new Array();
for (var i = 0; i<3; i++) {
	grid[i] = new Array;
}
//and set each square to 0, which is empty
for (var i = 0; i<3; i++) {
	for (var j = 0; j<3; j++) {
		grid[i][j]=0;
	}
}

// grid[0][1]=1;
// grid[2][2]=2;

var updateFps = function(delta) {
	fps = (1000/delta).toFixed(0);
}

// HERE EVENT LISTENERS SECTION

//mouse position event listener updates mouse position
function getMousePos(mycanvas, evt) {
	var rect = mycanvas.getBoundingClientRect();
	mouse.posY= evt.clientY - rect.top;
	mouse.posX= evt.clientX - rect.left; 
}

canvas.addEventListener('mousemove', function(evt) {getMousePos(canvas, evt);}, false);



//here mouse click listener, which adds signs in the grid on proper click

var whichSquare = function() { 
	//carefull, it can return null, handle exception properly!
	var x = null;
	var y = null;
	
	if (
		(2/24*canvas.width<mouse.posX) && 
		(mouse.posX<canvas.width*8/24+canvas.width*1/24/4)
	) {
		x = 0;
	}

	if (
		(2/24*canvas.height<mouse.posY) && 
		(mouse.posY<canvas.height*8/24+canvas.height*1/24/4)
	) {
		y = 0;
	}

	if (
		(2/24*canvas.width+7/24*canvas.width<mouse.posX) && 
		(mouse.posX<canvas.width*8/24+canvas.width*1/24/4+7/24*canvas.width)
	) {
		x = 1;
	}

	if (
		(2/24*canvas.height+7/24*canvas.height<mouse.posY) && 
		(mouse.posY<canvas.height*8/24+canvas.height*1/24/4+7/24*canvas.height)
	) {
		y = 1;
	}

	if (
		(2/24*canvas.width+14/24*canvas.width<mouse.posX) && 
		(mouse.posX<canvas.width*8/24+canvas.width*1/24/4+14/24*canvas.width)
	) {
		x = 2;
	}

	if (
		(2/24*canvas.height+14/24*canvas.height<mouse.posY) && 
		(mouse.posY<canvas.height*8/24+canvas.height*1/24/4+14/24*canvas.height)
	) {
		y = 2;
	}
	
	return [x, y]		
}

var addSign = function() {
	//add cross to the the value returned by whichSquare
	if ((whichSquare()[0]!=null) && (whichSquare()[1]!=null)) { //import: null exception must be handled
		var squareX = whichSquare()[0];
		var squareY = whichSquare()[1];
		//check if the square is empty
		if (grid[squareX][squareY]==0) {
			//write X or O based on var turn
			if (turn==1) {
				grid[squareX][squareY]=1;
				turn = 2;
			} else {
				grid[squareX][squareY]=2;
				turn = 1;

			}
		}
	}	
}

// event listener adds the sign on click
canvas.addEventListener('click', function(evt) {addSign();}, false);



//update game variable based on players action
var update = function(delta) {
	
}

// renders the updated game variable
var render = function(positionX, sqY) {

	//function to draw an X in a certain position
	var drawCross = function(pointX, pointY, boolMatchOver) {
		// //debug only!!!
		// ctx.fillStyle = "#444444"; //debug only!!!
		// ctx.fillRect(pointX, pointY, canvas.width*5/24, canvas.height*5/24);

		

		//here the cross
		var parts = 9;

		// pick gray color if match is over
		if (boolMatchOver) {
			ctx.fillStyle = col.whiteDark;
		} else {
			ctx.fillStyle = col.whiteLight;
		}

		for (var x=0; x<parts; x++) {
			ctx.fillRect(pointX+x*canvas.width*1/24*5/parts, pointY+x*canvas.width*1/24*5/parts, canvas.width*1/24*5/parts, canvas.height*1/24*5/parts);
			ctx.fillRect(pointX+x*canvas.width*1/24*5/parts, pointY+canvas.width*(parts-1-x)/24*5/parts, canvas.width*1/24*5/parts, canvas.height*1/24*5/parts);	
		}
		
	}

	var drawCircleNineParts = function(pointX, pointY, boolMatchOver) {
		// //debug only!!!
		// ctx.fillStyle = "#444444"; //debug only!!!
		// ctx.fillRect(pointX, pointY, canvas.width*5/24, canvas.height*5/24);

		if (boolMatchOver) {
			ctx.fillStyle = col.whiteDark;
		} else {
			ctx.fillStyle = col.whiteLight;
		}

		var pixel = nFlr(5/24*canvas.width/9) //nine parts

		ctx.fillRect(
			nFlr(pointX+2*pixel), 
			nFlr(pointY), 
			nFlr(pixel*5), 
			nFlr(pixel)
		); //top
		ctx.fillRect(
			nFlr(pointX+2*pixel), 
			nFlr(pointY+8*pixel), 
			nFlr(pixel*5), 
			nFlr(pixel)
		); //bottom
		ctx.fillRect(
			nFlr(pointX), 
			nFlr(pointY+2*pixel), 
			nFlr(pixel), 
			nFlr(pixel*5)
		);//left
		ctx.fillRect(
			nFlr(pointX+8*pixel), 
			nFlr(pointY+2*pixel), 
			nFlr(pixel), 
			nFlr(pixel*5)
		); //right
		

		//top left corner
		ctx.fillRect(nFlr(pointX+pixel), nFlr(pointY+pixel), nFlr(pixel*2), nFlr(pixel)); 
		ctx.fillRect(nFlr(pointX+pixel), nFlr(pointY+pixel), nFlr(pixel), nFlr(pixel*2));
		//top right corner
		ctx.fillRect(nFlr(pointX+6*pixel), nFlr(pointY+pixel), nFlr(pixel*2), nFlr(pixel)); 
		ctx.fillRect(nFlr(pointX+7*pixel), nFlr(pointY+pixel), nFlr(pixel), nFlr(pixel*2));
		//bottom left corner
		ctx.fillRect(nFlr(pointX+pixel), nFlr(pointY+6*pixel), nFlr(pixel), nFlr(pixel*2)); 
		ctx.fillRect(nFlr(pointX+pixel), nFlr(pointY+7*pixel), nFlr(pixel*2), nFlr(pixel));
		//bottom right corner
		ctx.fillRect(nFlr(pointX+7*pixel), nFlr(pointY+6*pixel), nFlr(pixel), nFlr(pixel*2)); 
		ctx.fillRect(nFlr(pointX+6*pixel), nFlr(pointY+7*pixel), nFlr(pixel*2), nFlr(pixel));
 	
	}

	var pxdim = Math.floor(canvas.width/96);

	//draw background
	ctx.fillStyle = col.black;
	ctx.fillRect(0, 0, canvas.width, canvas.height);


	//draw a pixel mouse
	ctx.fillStyle = col.whiteLight
	
	//top part of the arrow
	ctx.fillRect(mouse.posX, mouse.posY, pxdim, pxdim*10);
	for (var i=0; i<10; i++) {
		ctx.fillRect(mouse.posX+pxdim*i, mouse.posY+pxdim*i, pxdim, pxdim);
	};
	//bottomleft part of the arrow.
	ctx.fillStyle = col.whiteLight;
	// ctx.fillStyle = "#FF0000";
	ctx.fillRect(mouse.posX, mouse.posY+10*pxdim, pxdim, pxdim*10/2);
	for (var i=0; i<10/2; i++) {
		ctx.fillRect(mouse.posX+pxdim*i, mouse.posY+10*pxdim+pxdim*10/2-pxdim*i-pxdim, pxdim, pxdim);
	};
	//bottomright part of the arrow.
	ctx.fillStyle = col.whiteLight;
	//ctx.fillStyle = "#00FF00";
	ctx.fillRect(mouse.posX+10/2*pxdim, mouse.posY+10*pxdim, pxdim*10/2+pxdim, pxdim);


	//draw mouse coords
	ctx.fillStyle = "#444444";
	ctx.textAlign = "right";
	ctx.font="15px Courier";
	ctx.fillText("Mouse position: "+ mouse.posX +"; "+ mouse.posY, canvas.width*4/5, canvas.height/12);

	//draw fps
	ctx.fillStyle = "#444444";
	ctx.textAlign = "right";
	ctx.font="15px Courier";
	ctx.fillText("Fps: "+ fps, canvas.width*4/5, canvas.height/12+20);


	//draw the grid
	// check if match is over and set color
	if (bIsMatchOver) {
		ctx.fillStyle = col.whiteDark;
	} else {
		ctx.fillStyle = col.whiteLight;
	}

	//horizontal
	ctx.fillRect(canvas.width*2/24, canvas.height*8/24+canvas.width*1/24/4, canvas.width*20/24, canvas.height*1/24/2);
	ctx.fillRect(canvas.width*2/24, canvas.height*15/24+canvas.width*1/24/4, canvas.width*20/24, canvas.height*1/24/2);
	//vertical
	ctx.fillRect(canvas.width*8/24+canvas.width*1/24/4, canvas.height*2/24, canvas.width*1/24/2, canvas.height*20/24);
	ctx.fillRect(canvas.width*15/24+canvas.width*1/24/4, canvas.height*2/24, canvas.width*1/24/2, canvas.height*20/24);

	//let's fill in the grid with squares
	ctx.fillStyle = col.whiteLight;

	
	// //draw in a single slot
	// sqX = 0
	// sqY = 1
	// if (grid[sqY][sqY]==1) {
	// 	drawCross(canvas.width*(sqX*7/24+5/48), canvas.height*(sqY*7/24+5/48));
	// } else if (grid[sqY][sqY]==2) {
	// 	drawCircleNineParts(canvas.width*(sqX*7/24+5/48), canvas.height*(sqY*7/24+5/48));
	// }
	
	// draw all slots
	// for (var sqX=0; sqX<3; sqX++) {
	// 	for (var sqY=0; sqY<3; sqY++) {
	// 		// ctx.fillRect(canvas.width*(sqX*7/24+5/48), canvas.height*(sqY*7/24+5/48), canvas.width*5/24, canvas.height*5/24);
	// 		//drawCross(canvas.width*(sqX*7/24+5/48), canvas.height*(sqY*7/24+5/48));
	// 		drawCircleNineParts(canvas.width*(sqX*7/24+5/48), canvas.height*(sqY*7/24+5/48));
	// 	}
	// }


	//draw in all slots
	for (var sqX=0; sqX<3; sqX++) {
		for (var sqY=0; sqY<3; sqY++) {
			if (grid[sqX][sqY]==1) {
				drawCross(canvas.width*(sqX*7/24+5/48), canvas.height*(sqY*7/24+5/48), false);
			} else if (grid[sqX][sqY]==2) {
				drawCircleNineParts(canvas.width*(sqX*7/24+5/48), canvas.height*(sqY*7/24+5/48), false);
			}
		}
	}

	

}


//main loop
var mainLoop = function () {
	var now = Date.now();
	var delta = now-before;

	//here the functions to repeat every loop
	update();
	updateFps(delta);
	render();
	
	before=now;
}


var before = Date.now();
var gameInterval = setInterval(mainLoop, 1); //1 = one millisecond. Set as low as possible for fast execution. (every millisec.)