/* Mouse even handler*/

// Create the canvas
var canvas = document.createElement("canvas");
var ctx = canvas.getContext("2d");
canvas.width = 500;
canvas.height = 500;
canvas.id = "theCanvas";
//document.body.setAttribute("style", "background-color: pink");
document.body.appendChild(canvas);

//hide mouse while on canvas
document.getElementById('theCanvas').style.cursor = "none";

// GLOBALS
var mouse = {
	posX: 0,
	posY: 0,
};

zz

function getMousePos(canvas, evt) {
	var rect = canvas.getBoundingClientRect();
	return {
		x: evt.clientX - rect.left,
		y: evt.clientY - rect.top
	};
}



var update = function() {
	canvas.addEventListener('mousemove', function(evt) {
		var mousePos = getMousePos(canvas, evt);
		mouse.posX = mousePos.x;
		mouse.posY = mousePos.y;
	}, false);
}


var render = function() {

	//draw background
	ctx.fillStyle = "#000000";
	ctx.fillRect(0, 0, canvas.width, canvas.height);

	//draw a pixel mouse
	ctx.fillStyle = "#ffffff"
	var pxdim = 5;
	//top part of the arrow
	ctx.fillRect(mouse.posX, mouse.posY, pxdim, pxdim*10);
	for (var i=0; i<10; i++) {
		ctx.fillRect(mouse.posX+pxdim*i, mouse.posY+pxdim*i, pxdim, pxdim);
	};
	//bottomleft part of the arrow.
	ctx.fillStyle = "#FF0000";
	ctx.fillRect(mouse.posX, mouse.posY+10*pxdim, pxdim, pxdim*10/2);
	for (var i=0; i<10/2; i++) {
		ctx.fillRect(mouse.posX+pxdim*i, mouse.posY+10*pxdim+pxdim*10/2-pxdim*i-pxdim, pxdim, pxdim);
	};
	//bottomleft part of the arrow.
	ctx.fillStyle = "#00FF00";
	ctx.fillRect(mouse.posX+10/2*pxdim, mouse.posY+10*pxdim, pxdim*10/2, pxdim);
	
}


//main loop
var mainLoop = function () {
	var now = Date.now();
	var delta = now-before;

	//here the functions to repeat every loop
	update();
	render();

	before=now;
}


var before = Date.now();
var gameInterval = setInterval(mainLoop, 1); //1 = one millisecond. Set as low as possible for fast execution. (every millisec.)