/* Mouse even handler*/

// Create the canvas
var canvas = document.createElement("canvas");
var ctx = canvas.getContext("2d");
canvas.width = 600;
canvas.height = 600;
canvas.id = "theCanvas";
//document.body.setAttribute("style", "background-color: pink");
document.body.appendChild(canvas);

//hide mouse while on canvas
document.getElementById('theCanvas').style.cursor = "none";


// GLOBALS OUT OF THE LOOP
var fps = 0;


// GAME OBJECTS
var mouse = {
	posX: 1,
	posY: 1,
}



//grid [0 = empty, 1 = X, 2 = O]
var grid = new Array();

for (var i = 0; i<9; i++) {
	grid[i] = 0;
}


function getMousePos(mycanvas, evt) {
	var rect = mycanvas.getBoundingClientRect();
	mouse.posY= evt.clientY - rect.top;
	mouse.posX= evt.clientX - rect.left; 
}

canvas.addEventListener('mousemove', function(evt) {getMousePos(canvas, evt);}, false);


var updateFps = function(delta) {

	fps = (1000/delta).toFixed(0);

}


var render = function() {

	var pxdim = 5;

	//draw background
	ctx.fillStyle = "#000000";
	ctx.fillRect(0, 0, canvas.width, canvas.height);


	//draw a pixel mouse
	ctx.fillStyle = "#ffffff"
	
	//top part of the arrow
	ctx.fillRect(mouse.posX, mouse.posY, pxdim, pxdim*10);
	for (var i=0; i<10; i++) {
		ctx.fillRect(mouse.posX+pxdim*i, mouse.posY+pxdim*i, pxdim, pxdim);
	};
	//bottomleft part of the arrow.
	ctx.fillStyle = "#ffffff";
	// ctx.fillStyle = "#FF0000";
	ctx.fillRect(mouse.posX, mouse.posY+10*pxdim, pxdim, pxdim*10/2);
	for (var i=0; i<10/2; i++) {
		ctx.fillRect(mouse.posX+pxdim*i, mouse.posY+10*pxdim+pxdim*10/2-pxdim*i-pxdim, pxdim, pxdim);
	};
	//bottomright part of the arrow.
	ctx.fillStyle = "#ffffff";
	//ctx.fillStyle = "#00FF00";
	ctx.fillRect(mouse.posX+10/2*pxdim, mouse.posY+10*pxdim, pxdim*10/2+pxdim, pxdim);


	//draw mouse coords
	ctx.fillStyle = "#444444";
	ctx.textAlign = "right";
	ctx.font="15px Courier";
	ctx.fillText("Mouse position: "+ mouse.posX +"; "+ mouse.posY, canvas.width*4/5, canvas.height/12);

	//draw fps
	ctx.fillStyle = "#444444";
	ctx.textAlign = "right";
	ctx.font="15px Courier";
	ctx.fillText("Fps: "+ fps, canvas.width*4/5, canvas.height/12+20);


	//draw the grid
	ctx.fillStyle = "#ffffff"
	//horizontal
	ctx.fillRect(canvas.width*2/24, canvas.height*8/24, canvas.width*20/24, canvas.height*1/24/2);
	ctx.fillRect(canvas.width*2/24, canvas.height*15/24, canvas.width*20/24, canvas.height*1/24/2);
	//vertical
	ctx.fillRect(canvas.width*8/24, canvas.height*2/24, canvas.width*1/24/2, canvas.height*20/24);
	ctx.fillRect(canvas.width*15/24, canvas.height*2/24, canvas.width*1/24/2, canvas.height*20/24);

	
}


//main loop
var mainLoop = function () {
	var now = Date.now();
	var delta = now-before;

	//here the functions to repeat every loop
	// update();
	updateFps(delta);
	render();
	
	before=now;
}


var before = Date.now();
var gameInterval = setInterval(mainLoop, 1); //1 = one millisecond. Set as low as possible for fast execution. (every millisec.)