// Create the canvas
var canvas = document.createElement("canvas");
var ctx = canvas.getContext("2d");
canvas.width = 256;
canvas.height = 256;
document.body.appendChild(canvas);

//Give me a random number function min included max included
function random(min, max) {
		rand=Math.floor((max-min+1)*Math.random())+min;   
		return rand;
}


//load img background 'desert'
var desertReady = false;
var desertImg = new Image();
desertImg.onload = function() {
	desertReady = true;
}
desertImg.src = "images/desert.png";

var dudeReady = false;
var dudeImg = new Image();
dudeImg.onload = function() {
	dudeReady = true;
}
dudeImg.width=32;
dudeImg.height=32;
dudeImg.src = "images/dude.png";

var waterReady = false;
var waterImg = new Image();
waterImg.onload = function() {
	waterReady = true;
}
waterImg.src = "images/water.png";


//objects
var dude = {speed:150, x:(canvas.width-dudeImg.width)/2, y:(canvas.height-dudeImg.height)/2};
var water = {x:0, y:0};

//key handler
var keysDown = {};

addEventListener("keydown", function (e) {keysDown[e.keyCode] = true;}, false);
addEventListener("keyup", function (e) {delete keysDown[e.keyCode];}, false);

//reset
var reset = function() {
	/*dude.x = canvas.width/2-dudeImg.width/2;
	dude.y = canvas.height/2-dudeImg.height/2;*/
	
	var randomX = 0;
	var randomY = 0;
	
	do {
		randomX = random(0, 224);
		randomY = random(0, 224);
	} while ( 
		(dude.x+32 >= randomX) &&
		(dude.y+32 >= randomY) &&
		(randomX+32 >= dude.x) &&
		(randomY+32 >= dude.y)
	);
	
	water.x = randomX;
	water.y = randomY;

}

//update: changes the pg movement
var update = function(deltaT) {

	if (dude.y>0) {if ((38 in keysDown)&&!(37 in keysDown)&&!(39 in keysDown)) {dude.y-=dude.speed*deltaT}} //38 up arrow
	if (dude.y<224) {if ((40 in keysDown)&&!(37 in keysDown)&&!(39 in keysDown)) {dude.y+=dude.speed*deltaT}} //40 down arrow
	if (dude.x>0) {if ((37 in keysDown)&&!(38 in keysDown)&&!(40 in keysDown)) {dude.x-=dude.speed*deltaT}} //37 left arrow
	if (dude.x<224) {if ((39 in keysDown)&&!(38 in keysDown)&&!(40 in keysDown)) {dude.x+=dude.speed*deltaT}} //39 right arrow
	
	
	if ((dude.y>0)&&(dude.x>0)) {if ((37 in keysDown)&&(38 in keysDown)) {dude.x-=0.71*dude.speed*deltaT; dude.y-=0.71*dude.speed*deltaT;}} //up left
	if ((dude.y>0)&&(dude.x<224)) {if ((38 in keysDown)&&(39 in keysDown)) {dude.x+=0.71*dude.speed*deltaT; dude.y-=0.71*dude.speed*deltaT;}} // up right
	if ((dude.y<224)&&(dude.x<224)) {if ((39 in keysDown)&&(40 in keysDown)) {dude.x+=0.71*dude.speed*deltaT; dude.y+=0.71*dude.speed*deltaT;}} // down right
	if ((dude.y<224)&&(dude.x>0)) {if ((37 in keysDown)&&(40 in keysDown)) {dude.x-=0.71*dude.speed*deltaT; dude.y+=0.71*dude.speed*deltaT;}} // down left
	
	
	if ( 
		(dude.x+32 >= water.x) &&
		(dude.y+32 >= water.y) &&
		(water.x+32 >= dude.x) &&
		(water.y+32 >= dude.y)
	) {
		reset();
	}
	
	
	
	
	
}

//draw imgs
var render = function() {
	if(desertReady) {
		ctx.drawImage(desertImg, 0, 0);
	}
	
	if(dudeReady) {
		ctx.drawImage(dudeImg, dude.x, dude.y);
	}
	
	if(waterReady) {
		ctx.drawImage(waterImg, water.x, water.y);
	}
}



//still needs speed modifier
// The main game loop
var main = function () {
	var now = Date.now();
	var delta = now-before;
	//reset();
	update(delta/1000);
	render();
	
	before=now;
};

var before = Date.now(); //first time 
// Let's play this game!
setInterval(main, 1); // Execute as fast as possible